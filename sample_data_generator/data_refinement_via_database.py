#!/usr/bin/env python
# coding: utf-8

# In[40]:


# connect_db：连接数据库，并操作数据库
 
import pymysql
 
 
class OperationMysql:
    """
    数据库SQL相关操作
    import pymysql
# 打开数据库连接
db = pymysql.connect("localhost","testuser","test123","TESTDB" )
# 使用 cursor() 方法创建一个游标对象 cursor
cursor = db.cursor()
# 使用 execute()  方法执行 SQL 查询
cursor.execute("SELECT VERSION()")
    """
 
    def __init__(self):
        # 创建一个连接数据库的对象
        self.conn = pymysql.connect(
            host='sh-cynosdbmysql-grp-l7dozfme.sql.tencentcdb.com',  # 连接的数据库服务器主机名
            port=29815,  # 数据库端口号
            user='test',  # 数据库登录用户名
            passwd='PoeticJourney()~',
            db='poetic_journey',  # 数据库名称
            charset='utf8',  # 连接编码
            cursorclass=pymysql.cursors.DictCursor
        )
        # 使用cursor()方法创建一个游标对象，用于操作数据库
        self.cur = self.conn.cursor()
 
    # 查询一条数据
    def search_one(self, sql):
        self.cur.execute(sql)
        result = self.cur.fetchone()  # 使用 fetchone()方法获取单条数据.只显示一行结果
        # result = self.cur.fetchall()  # 显示所有结果
        return result
 
    # 更新SQL
    def updata_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()  # 记得关闭数据库连接
 
    # 插入SQL
    def insert_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()
 
    # 删除sql
    def delete_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()
#数据库与xlsx同时操作
import pandas as pd
import numpy as np
data = pd.read_excel(".\cleaned_comment\BeiJing_comment_clean.xlsx", sheet_name = "Sheet1") 
 
if __name__ == '__main__':
    op_mysql = OperationMysql()
    for i in range(0,len(data),1):
        temp=op_mysql.search_one("SELECT scenery_id from scenery WHERE scenery_name like '%"+data.iloc[i,1]+"'")
        if temp==None:
            continue
        data.iloc[i,0]=temp["scenery_id"]
        data.iloc[i,5]=i+1
        print(i)
    data.to_excel('.\cleaned_comment\BeiJing_comment_final.xlsx') 
    print("Finish")


# In[30]:


print(data.iloc[2314,:])


# In[39]:


temp=op_mysql.search_one("SELECT scenery_id from scenery WHERE scenery_name like '%"+data.iloc[2314,1]+"'")
print(temp==None)


# In[41]:


import pymysql
 
 
class OperationMysql:
    """
    数据库SQL相关操作
    import pymysql
# 打开数据库连接
db = pymysql.connect("localhost","testuser","test123","TESTDB" )
# 使用 cursor() 方法创建一个游标对象 cursor
cursor = db.cursor()
# 使用 execute()  方法执行 SQL 查询
cursor.execute("SELECT VERSION()")
    """
 
    def __init__(self):
        # 创建一个连接数据库的对象
        self.conn = pymysql.connect(
            host='sh-cynosdbmysql-grp-l7dozfme.sql.tencentcdb.com',  # 连接的数据库服务器主机名
            port=29815,  # 数据库端口号
            user='test',  # 数据库登录用户名
            passwd='PoeticJourney()~',
            db='poetic_journey',  # 数据库名称
            charset='utf8',  # 连接编码
            cursorclass=pymysql.cursors.DictCursor
        )
        # 使用cursor()方法创建一个游标对象，用于操作数据库
        self.cur = self.conn.cursor()
 
    # 查询一条数据
    def search_one(self, sql):
        self.cur.execute(sql)
        result = self.cur.fetchone()  # 使用 fetchone()方法获取单条数据.只显示一行结果
        # result = self.cur.fetchall()  # 显示所有结果
        return result
 
    # 更新SQL
    def updata_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()  # 记得关闭数据库连接
 
    # 插入SQL
    def insert_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()
 
    # 删除sql
    def delete_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()
#数据库与xlsx同时操作
import pandas as pd
import numpy as np
data = pd.read_excel(".\cleaned_comment\BeiJing_comment_clean_2.xlsx", sheet_name = "Sheet1") 
 
if __name__ == '__main__':
    op_mysql = OperationMysql()
    for i in range(0,len(data),1):
        temp=op_mysql.search_one("SELECT scenery_id from scenery WHERE scenery_name like '%"+data.iloc[i,1]+"'")
        if temp==None:
            continue
        data.iloc[i,0]=temp["scenery_id"]
        data.iloc[i,5]=i+1
    data.to_excel('.\cleaned_comment\BeiJing_comment_final_2.xlsx') 
    print("Finish")


# In[25]:


import pymysql
 
 
class OperationMysql:
    """
    数据库SQL相关操作
    import pymysql
# 打开数据库连接
db = pymysql.connect("localhost","testuser","test123","TESTDB" )
# 使用 cursor() 方法创建一个游标对象 cursor
cursor = db.cursor()
# 使用 execute()  方法执行 SQL 查询
cursor.execute("SELECT VERSION()")
    """
 
    def __init__(self):
        # 创建一个连接数据库的对象
        self.conn = pymysql.connect(
            host='sh-cynosdbmysql-grp-l7dozfme.sql.tencentcdb.com',  # 连接的数据库服务器主机名
            port=29815,  # 数据库端口号
            user='test',  # 数据库登录用户名
            passwd='PoeticJourney()~',
            db='poetic_journey',  # 数据库名称
            charset='utf8',  # 连接编码
            cursorclass=pymysql.cursors.DictCursor
        )
        # 使用cursor()方法创建一个游标对象，用于操作数据库
        self.cur = self.conn.cursor()
 
    # 查询一条数据
    def search_one(self, sql):
        self.cur.execute(sql)
        result = self.cur.fetchone()  # 使用 fetchone()方法获取单条数据.只显示一行结果
        # result = self.cur.fetchall()  # 显示所有结果
        return result
 
    # 更新SQL
    def updata_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()  # 记得关闭数据库连接
 
    # 插入SQL
    def insert_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()
 
    # 删除sql
    def delete_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()
#数据库与xlsx同时操作
import pandas as pd
import numpy as np
data = pd.read_excel(".\cleaned_comment\BeiJing_comment_clean_3.xlsx", sheet_name = "Sheet1") 
 
if __name__ == '__main__':
    op_mysql = OperationMysql()
    for i in range(0,len(data),1):
        string=data.iloc[i,1]
        isexists=False
        for j in range(0,len(string),1):
            if string[j]=='\'':
                isexists=True
                first,second=string.split('\'',1)
                final=first+'\\\''+second
        if  isexists==True:
            temp=op_mysql.search_one("SELECT scenery_id from scenery WHERE scenery_name like '%"+final+"'")
        else:
            temp=op_mysql.search_one("SELECT scenery_id from scenery WHERE scenery_name like '%"+str(data.iloc[i,1])+"'")
        if temp==None:
            continue
        data.iloc[i,0]=temp["scenery_id"]
        data.iloc[i,5]=i+1+7430
        print(i)
    data.to_excel('.\cleaned_comment\BeiJing_comment_final_3.xlsx') 
    print("Finish")


# In[19]:


import pandas as pd
import numpy as np

data = pd.read_excel(".\cleaned_comment\BeiJing_comment_clean_3.xlsx", sheet_name = "Sheet1") 
# print(str(data.iloc[348,1]))
# print(temp["scenery_id"])
for i in range(0,len(data.iloc[348,1]),1):
    if data.iloc[348,1][i]=='\'':
        first,second=data.iloc[348,1].split('\'',1)
        final=first+'\\\''+second
print(final)
op_mysql = OperationMysql()
temp=op_mysql.search_one("SELECT scenery_id from scenery WHERE scenery_name like '%"+final+"'")
print(temp["scenery_id"])


# In[26]:


import pymysql
 
 
class OperationMysql:
    """
    数据库SQL相关操作
    import pymysql
# 打开数据库连接
db = pymysql.connect("localhost","testuser","test123","TESTDB" )
# 使用 cursor() 方法创建一个游标对象 cursor
cursor = db.cursor()
# 使用 execute()  方法执行 SQL 查询
cursor.execute("SELECT VERSION()")
    """
 
    def __init__(self):
        # 创建一个连接数据库的对象
        self.conn = pymysql.connect(
            host='sh-cynosdbmysql-grp-l7dozfme.sql.tencentcdb.com',  # 连接的数据库服务器主机名
            port=29815,  # 数据库端口号
            user='test',  # 数据库登录用户名
            passwd='PoeticJourney()~',
            db='poetic_journey',  # 数据库名称
            charset='utf8',  # 连接编码
            cursorclass=pymysql.cursors.DictCursor
        )
        # 使用cursor()方法创建一个游标对象，用于操作数据库
        self.cur = self.conn.cursor()
 
    # 查询一条数据
    def search_one(self, sql):
        self.cur.execute(sql)
        result = self.cur.fetchone()  # 使用 fetchone()方法获取单条数据.只显示一行结果
        # result = self.cur.fetchall()  # 显示所有结果
        return result
 
    # 更新SQL
    def updata_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()  # 记得关闭数据库连接
 
    # 插入SQL
    def insert_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()
 
    # 删除sql
    def delete_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()
#数据库与xlsx同时操作
import pandas as pd
import numpy as np
data = pd.read_excel(".\cleaned_comment\BeiJing_comment_clean_4.xlsx", sheet_name = "Sheet1") 
 
if __name__ == '__main__':
    op_mysql = OperationMysql()
    for i in range(0,len(data),1):
        string=data.iloc[i,1]
        isexists=False
        for j in range(0,len(string),1):
            if string[j]=='\'':
                isexists=True
                first,second=string.split('\'',1)
                final=first+'\\\''+second
        if  isexists==True:
            temp=op_mysql.search_one("SELECT scenery_id from scenery WHERE scenery_name like '%"+final+"'")
        else:
            temp=op_mysql.search_one("SELECT scenery_id from scenery WHERE scenery_name like '%"+str(data.iloc[i,1])+"'")
        if temp==None:
            continue
        data.iloc[i,0]=temp["scenery_id"]
        data.iloc[i,5]=i+1+10450
        print(i)
    data.to_excel('.\cleaned_comment\BeiJing_comment_final_4.xlsx') 
    print("Finish")


# In[27]:


import pymysql
 
 
class OperationMysql:
    """
    数据库SQL相关操作
    import pymysql
# 打开数据库连接
db = pymysql.connect("localhost","testuser","test123","TESTDB" )
# 使用 cursor() 方法创建一个游标对象 cursor
cursor = db.cursor()
# 使用 execute()  方法执行 SQL 查询
cursor.execute("SELECT VERSION()")
    """
 
    def __init__(self):
        # 创建一个连接数据库的对象
        self.conn = pymysql.connect(
            host='sh-cynosdbmysql-grp-l7dozfme.sql.tencentcdb.com',  # 连接的数据库服务器主机名
            port=29815,  # 数据库端口号
            user='test',  # 数据库登录用户名
            passwd='PoeticJourney()~',
            db='poetic_journey',  # 数据库名称
            charset='utf8',  # 连接编码
            cursorclass=pymysql.cursors.DictCursor
        )
        # 使用cursor()方法创建一个游标对象，用于操作数据库
        self.cur = self.conn.cursor()
 
    # 查询一条数据
    def search_one(self, sql):
        self.cur.execute(sql)
        result = self.cur.fetchone()  # 使用 fetchone()方法获取单条数据.只显示一行结果
        # result = self.cur.fetchall()  # 显示所有结果
        return result
 
    # 更新SQL
    def updata_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()  # 记得关闭数据库连接
 
    # 插入SQL
    def insert_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()
 
    # 删除sql
    def delete_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()
#数据库与xlsx同时操作
import pandas as pd
import numpy as np
data = pd.read_excel(".\cleaned_comment\HongKong_comment_clean_1.xlsx", sheet_name = "Sheet1") 
 
if __name__ == '__main__':
    op_mysql = OperationMysql()
    for i in range(0,len(data),1):
        string=data.iloc[i,1]
        isexists=False
        for j in range(0,len(string),1):
            if string[j]=='\'':
                isexists=True
                first,second=string.split('\'',1)
                final=first+'\\\''+second
        if  isexists==True:
            temp=op_mysql.search_one("SELECT scenery_id from scenery WHERE scenery_name like '%"+final+"'")
        else:
            temp=op_mysql.search_one("SELECT scenery_id from scenery WHERE scenery_name like '%"+str(data.iloc[i,1])+"'")
        if temp==None:
            continue
        data.iloc[i,0]=temp["scenery_id"]
        data.iloc[i,5]=i+1+13912
        print(i)
    data.to_excel('.\cleaned_comment\HongKong_comment_final_1.xlsx') 
    print("Finish")


# In[36]:


import pymysql
 
 
class OperationMysql:
    """
    数据库SQL相关操作
    import pymysql
# 打开数据库连接
db = pymysql.connect("localhost","testuser","test123","TESTDB" )
# 使用 cursor() 方法创建一个游标对象 cursor
cursor = db.cursor()
# 使用 execute()  方法执行 SQL 查询
cursor.execute("SELECT VERSION()")
    """
 
    def __init__(self):
        # 创建一个连接数据库的对象
        self.conn = pymysql.connect(
            host='sh-cynosdbmysql-grp-l7dozfme.sql.tencentcdb.com',  # 连接的数据库服务器主机名
            port=29815,  # 数据库端口号
            user='test',  # 数据库登录用户名
            passwd='PoeticJourney()~',
            db='poetic_journey',  # 数据库名称
            charset='utf8',  # 连接编码
            cursorclass=pymysql.cursors.DictCursor
        )
        # 使用cursor()方法创建一个游标对象，用于操作数据库
        self.cur = self.conn.cursor()
 
    # 查询一条数据
    def search_one(self, sql):
        self.cur.execute(sql)
        result = self.cur.fetchone()  # 使用 fetchone()方法获取单条数据.只显示一行结果
        # result = self.cur.fetchall()  # 显示所有结果
        return result
 
    # 更新SQL
    def updata_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()  # 记得关闭数据库连接
 
    # 插入SQL
    def insert_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()
 
    # 删除sql
    def delete_one(self, sql):
        try:
            self.cur.execute(sql)  # 执行sql
            self.conn.commit()  # 增删改操作完数据库后，需要执行提交操作
        except:
            # 发生错误时回滚
            self.conn.rollback()
        self.conn.close()
#数据库与xlsx同时操作
import pandas as pd
import numpy as np
data = pd.read_excel(".\cleaned_comment\Shanghai_clean.xlsx", sheet_name = "Sheet1") 
 
if __name__ == '__main__':
    op_mysql = OperationMysql()
    for i in range(0,len(data),1):
        string=data.iloc[i,1]
        for j in range(0,len(string),1):
            if string[j]>='A' and string[j]<='Z' or string[j]>='a' and string[j]<='z':
                final=string[0:j]
                break
        temp=op_mysql.search_one("SELECT scenery_id from scenery WHERE scenery_name like '%"+final+"'")
        if temp==None:
            continue
        data.iloc[i,0]=temp["scenery_id"]
        data.iloc[i,5]=i+1+29145
        print(i)
    data.to_excel('.\cleaned_comment\Shanghai_clean_final.xlsx') 
    print("Finish")


# In[35]:


string=data.iloc[1,1]
for j in range(0,len(string),1):
    if string[j]>='A' and string[j]<='Z' or string[j]>='a' and string[j]<='z':
        final=string[0:j]
        print(final)
        break
temp=op_mysql.search_one("SELECT scenery_id from scenery WHERE scenery_name like '%"+final+"'")
print(temp["scenery_id"])


# In[39]:


import pandas as pd
import numpy as np
data = pd.read_excel(".\cleaned_comment\Shanghai_clean_final.xlsx", sheet_name = "Sheet1") 
for i in range(0,len(data),1):
    if data.iloc[i,1]=="上海迪士尼乐团Shanghai Disneyland Band" :
        data.iloc[i,0]=22
    elif data.iloc[i,1]=="上海环球金融中心Shanghai World Financial Center":
        data.iloc[i,0]=220
    elif data.iloc[i,1]=="上海城隍庙道观City God Temple of Shanghai":
        data.iloc[i,0]=407
    elif data.iloc[i,1]=="朱家角古镇景区Zhujiajiao Ancient Town Scenic Area":
        data.iloc[i,0]=136
    elif data.iloc[i,1]=="七宝老街Qibao Old Street":
        data.iloc[i,0]=162
data=data.dropna(axis=0,how='all',subset=['scenery_id'], inplace=False)
data.to_excel('.\cleaned_comment\Shanghai_clean_final_2.xlsx') 


# In[41]:


import pandas as pd
import numpy as np
data = pd.read_excel(".\cleaned_comment\Shanghai_clean_final_2.xlsx", sheet_name = "Sheet1") 
for i in range(0,len(data),1):
    data.iloc[i,5]=i+29146
data.to_excel('.\cleaned_comment\Shanghai_clean_final_3.xlsx') 


# In[ ]:




