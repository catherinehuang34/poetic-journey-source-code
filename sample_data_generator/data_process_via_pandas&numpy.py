#!/usr/bin/env python
# coding: utf-8

# In[11]:


# import xlrd
# wb = xlrd.open_workbook(r'C:\Users\HUAWEI\Desktop\lately\database\course project\HongKong.xlsx', encoding_override='utf-8')
# st = wb.sheet_by_name('sheet1')
import pandas as pd
import numpy as np
data = pd.read_excel("HongKong_comment.xlsx", sheet_name = "Sheet1")
# print(data.iloc[12,7])
# print(data.iloc[12,7].isnull())
# for i in range(1,len(data.keys()):
#     print(data[i,:])
#     if data[1]==np.nan:
#     data.drop([i],axis = 0,inplace = True)
# nrows=st.nrows
# ncols=st.ncols
# for i in range(1,2,1):
#     for j in range(1,ncols,1):
#         print(st.cell_value(i,j))
df=data.dropna(axis=0,how='all',subset=['street'], inplace=False) #过滤空值过多的列
for i in range(0,len(df),1):
    temp=df.iloc[i,7]
    for j in range(0,len(df.iloc[i,7])-3,1):
        if(temp[j]=='市' and temp[j+3]=='区'):
            df.iloc[i,6]=temp[j+1:j+4]
            temp=temp[j+4:]
            df.iloc[i,7]=temp
            break;
df.to_excel('北京_clean.xlsx')


# In[32]:


import pandas as pd
import numpy as np
data = pd.read_excel("BeiJing_comment_2(2).xlsx", sheet_name = "Sheet1")
df=data.dropna(axis=0,how='all',subset=['title'], inplace=False) #过滤空值过多的列
for i in range(0,len(df),1):
    temp=df.iloc[i,1]
    flag=False
    #print(temp)
    for j in range(0,len(temp),1):
        temp_len=len(temp)
        if(temp[j]=='，' and j!=0):
            #print(temp)
            df.iloc[i,1]=temp[:j]
            temp=temp[j+1:]
            df.iloc[i,2]=temp
            flag=True
            break;
    if(flag==False):
        df.iloc[i,2]=df.iloc[i,1]
df.to_excel('.\cleaned_comment\BeiJing_comment_clean.xlsx')


# In[2]:


import pandas as pd
import numpy as np
data = pd.read_excel("Shanghai.xlsx", sheet_name = "Sheet1")
df=data.dropna(axis=0,how='all',subset=['title'], inplace=False)
for i in range(0,len(df),1):
    temp=df.iloc[i,2]
    flag=False
    #print(temp)
    for j in range(0,len(temp),1):
        #print(j)
        if(temp[j]=='，' and j!=0):
            #print(temp)
            df.iloc[i,2]=temp[:j]
            temp=temp[j+1:]
            df.iloc[i,3]=temp
            flag=True
            break;
    if(flag==False):
        df.iloc[i,2]=df.iloc[i,1]
for i in range(0,len(df),1):
    temp=df.iloc[i,1]
    if temp.startswith('...'):
        temp="\""+df.iloc[i,0]+"\""+temp[3:]
        df.iloc[i,1]=temp
df.to_excel('.\cleaned_comment\Shanghai_clean.xlsx')    


# In[1]:


import pandas as pd
import numpy as np
data = pd.read_excel("HongKong.xlsx", sheet_name = "Sheet1")
df=data.dropna(axis=0,how='all',subset=['street'], inplace=False)
data.to_excel('HongKong_new.xlsx') 


# In[30]:


import pandas as pd
import numpy as np
data = pd.read_csv("XiaMen_comment_new.csv")
df=data.dropna(axis=0,how='all',subset=['title'], inplace=False)
for i in range(0,len(df),1):
    temp=df.iloc[i,1]
    flag=False
    #print(temp)
    for j in range(0,len(temp),1):
        #print(j)
        if(temp[j]=='，' and j!=0):
            #print(temp)
            df.iloc[i,1]=temp[:j]
            temp=temp[j+1:]
            df.iloc[i,2]=temp
            flag=True
            break;
    if(flag==False):
        df.iloc[i,2]=df.iloc[i,1]
for i in range(0,len(df),1):
    temp=df.iloc[i,1]
    if temp.startswith('...'):
        temp="\""+df.iloc[i,0]+"\""+temp[3:]
        df.iloc[i,1]=temp
df.to_excel('.\cleaned_comment\XiaMen_comment_clean_3.xlsx')    


# In[27]:


import pandas as pd
import numpy as np
data = pd.read_excel("HongKong_comment_14k(2).xlsx")
#df=data.dropna(axis=0,how='all',subset=['title'], inplace=False)
writer = pd.ExcelWriter('.\cleaned_comment\HongKong_comment_clean_1.xlsx')
#df_new = pd.DataFrame(columns=["scenery_name","title","comment_text","commnet_time","user","picture"])
#df_new.to_excel(writer,sheet_name="Sheet1")
#data split
scenery_name=[]
title=[]
comment_text=[]
commnet_time=[]
user=[]
picture=[]
for i in range(5834,len(data),1):
    #if(df.iloc[i,0])
    scenery_name.append(data.iloc[i,0])
    title.append(data.iloc[i,1])
    comment_text.append(data.iloc[i,2])
    commnet_time.append(data.iloc[i,3])
    user.append(data.iloc[i,4])
    picture.append(data.iloc[i,6])
data_new={"scenery_name":scenery_name,"title":title,"comment_text":comment_text,"commnet_time":commnet_time,"user":user,"picture":picture}
data_new=pd.DataFrame(data_new)
data_new.to_excel(writer,sheet_name="Sheet1")
writer.save()
writer.close()


# In[23]:





# In[ ]:




