# poetic_journey

## 一些规范

（大概）

- URL中使用下划线
- CSS中使用小写字母+短横线
- component data中使用驼峰，首字母小写（需要后端转换）
- 即前端除了URL内容、CSS均使用驼峰（但props不能用驼峰？因为也不区分大小写）
- 后端数据除数据库外，使用驼峰，首字母大写
- get均使用params（axios或gin导致get不能接收json？），post均使用json

## TODO

**按钮颜色**
Element 的部分按钮（消息框、上床图片）颜色不知道为什么为灰色，找不到默认的修改位置。

只能这样改，但效果并不好：

```
.left-box .img-upload :deep() .el-button {
	color: black;
	border-color: rgb(64,158,255);
	background-color: rgb(64, 158, 255);
}
```

**消息提示**
上传超过6张图片时，多个Error只显示一个，但会导致多个空行。


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
