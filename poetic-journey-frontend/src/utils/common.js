const loginExpired = function(msg) {
	this.$router.push({
		name: 'Login'
	})
	ElMessage.error({
		title: '登录已过期，请重新登录',
		message: msg
	})
}
const deepCopy = function(item) {
	return JSON.parse(JSON.stringify(item))
}

// 将\n替换为<p>，然后使用返回的字符串替换 innerHTML
const paraSeparate = function(str) {
	const regex = new RegExp('\\n', 'g'); // g: global, 找某个模式的所有位置
	if (str == undefined) {
		return '<p>暂无内容</p>'
	}
	return '<p>' + str.replace(regex, '</p><p>') + '</p>';
}

export {
	loginExpired, deepCopy, paraSeparate,
}
