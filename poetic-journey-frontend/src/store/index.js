import { createStore } from 'vuex'

import user from './modules/user'

export default createStore({
	strict: true,
	state: {
	},
	mutations: {
	},
	actions: {
	},
	modules: {
		user
	}
})

// import App from './App.vue'
// const app = createApp(App)
// app.use(Vuex)
