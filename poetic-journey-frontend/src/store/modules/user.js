export default {
	state: {
		user: null, // 当前登录用户的基本信息（userID, username, avatar）
		userInfo: null, // 当前登录用户的详细信息（user表）
	},
	getters: {
		getUser(state) {
			return state.user
		},
		getUserInfo(state) {
			return state.userInfo
		},
		// 会变的信息，不存储在本地
		// getUserMsgNum(state) {
		// 	return state.user.id // todo
		// }
	},
	mutations: {
		setUser(state, data) {
			state.user = data
		},
		setUserInfo(state, data) {
			state.userInfo = data
		},
	},
	actions: {
		setUser({ commit }, data) {
			commit('setUser', data)
		},
		setUserInfo({ commit }, data) {
			commit('setUserInfo', data)
		},
	}
}
