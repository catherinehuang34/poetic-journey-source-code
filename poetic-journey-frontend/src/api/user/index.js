import axios from 'axios'
import qs from 'qs'

// 注册
const postRegister = form =>
	axios.post('/api/v1/register', form).then(res => res.data)

// 登录
const postLogin = form =>
	axios.post('/api/v1/login', form).then(res => res.data)

// 获取验证码
const getCaptcha = () =>
	axios.get('/api/v0/get_captcha').then(res => res.data)

// 检验token
const checkToken = () =>
	axios.get('/api/v1/check_token').then(res => res.data)

// 获取用户信息
// 注意 get params 使用form 而非json
const getUserInfo = userID =>
	axios.get('/api/v1/user_info', {
		params: {userID, }
	}).then(res => res.data)

// 获取多个用户的信息
const getUsersInfo = userIDs =>
	axios.get('/api/v1/users_info', {
		params: {userIDs, },
		paramsSerializer: params => {
			return qs.stringify(params, { indices: false })
		}
	}).then(res => res.data)

// 获取用户社交信息
const getUserSocialInfo = userID =>
	axios.get('/api/v1/user_social_info', {
		params: {userID, }
	}).then(res => res.data)

// 修改用户信息
const updateUserInfo = (userID, gender, age, province, city, district, brief, avatar) =>
	axios.post('/api/v1/user_info', {
		userID: userID,
		gender: gender,
		age: age,
		province: province,
		city: city,
		district: district,
		brief: brief,
		avatar: avatar,
	}).then(res => res.data)

export {
	postRegister, postLogin,
	getCaptcha,
	checkToken,

	getUserInfo, getUsersInfo, getUserSocialInfo,

	updateUserInfo,
}
