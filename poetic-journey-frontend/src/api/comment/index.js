import axios from 'axios'

// 获取卡片评论列表
const getComments = (cardID) =>
	axios.get('/api/v1/comments', {
		params: { cardID, }
	}).then(res => res.data)

// 发表对卡片的评论
const postComment = (cardID, userID, commentText) =>
	axios.post('/api/v1/comment', {
		cardID: cardID,
		userID: userID,
		commentText: commentText,
	}).then(res => res.data)

export {
	getComments,
	postComment,
}
