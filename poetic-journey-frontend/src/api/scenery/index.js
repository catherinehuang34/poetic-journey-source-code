import axios from 'axios'

// 新建景点
const createScenery = (sceneryName, province, city, district, street, intro, openTime, price, score, pics) =>
	axios.post(`/api/v1/scenery`, {
		sceneryName: sceneryName,
		province: province,
		city: city,
		district: district,
		street: street,
		intro: intro,
		openTime: openTime,
		price: price,
		score: score,
		pics: pics,
	}).then(res => res.data)

// 获取景点所有信息
// 返回 Scenery
const getSceneryByID = (sceneryID) =>
	axios.get(`/api/v1/scenery_by_id`, {
		params: {
			sceneryID,
		}
	}).then(res => res.data)

// 获取景点列表（无需包含完整的省、市、区、具体内容，模糊查找由后端实现）
// 如果不包含市、区，仍可包含具体内容（准确名称）
// 返回 SceneryBrief
const getSceneryByDetail = (province, city, district, keyword, offset, limit) =>
	axios.get(`/api/v1/scenery_by_detail`, {
		params: {
			province, city, district, keyword, offset, limit,
		}
	}).then(res => res.data)

// 获取景点列表（无需包含完整的省、市、区、具体内容，模糊查找由后端实现）
// 如果不包含市、区，仍可包含tag
// 返回 SceneryBrief
const getSceneryByTag = (province, city, district, tag, offset, limit) =>
	axios.get(`/api/v1/scenery_by_tag`, {
		params: {
			province, city, district, tag, offset, limit,
		}
	}).then(res => res.data)

// 获取景点列表（仅包含ID和名称，用于展示列表）
// 返回 SceneryName
const getSceneryNameByDetail = (province, city, district, keyword, offset, limit) =>
	axios.get(`/api/v1/scenery_name_by_detail`, {
		params: {
			province, city, district, keyword, offset, limit,
		}
	}).then(res => res.data)

export {
	createScenery,

	getSceneryByID, getSceneryByDetail, getSceneryByTag,
	getSceneryNameByDetail,
}
