import axios from 'axios'

// 注册
const uploadImage = filename =>
	axios.post('/api/v1/upload_image', { filename: filename })
	.then(res => res.data)

export {
	uploadImage,
}
