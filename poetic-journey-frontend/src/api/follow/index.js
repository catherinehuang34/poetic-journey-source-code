import axios from 'axios'
import qs from 'qs'

// 获取关注列表(type 0:follower 1:followee)
const getFollowList = (userID, type, offset, limit) =>
	axios.get('/api/v1/follow_list', {
		params: {userID, type, offset, limit,}
	}).then(res => res.data)

// 关注(type 0:follow 1:unfollow)
const followUser = (followerID, followeeID, type) =>
	axios.post('/api/v1/follow_user', {
		followerID: followerID,
		followeeID: followeeID,
		type: type,
	}).then(res => res.data)

// 是否关注某些用户
const checkFollowUser = (userID, targetIDs) =>
	axios.get('/api/v1/follow_check', {
		params: {userID, targetIDs,},
		paramsSerializer: params => {
			return qs.stringify(params, { indices: false })
		}
	}).then(res => res.data)

export {
	getFollowList,
	followUser, checkFollowUser,
}
