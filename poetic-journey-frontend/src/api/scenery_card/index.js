import axios from 'axios'
import qs from 'qs'

// 推送卡片，返回卡片列表（包含简略信息）
const pushSceneryCards = (userID, province, city, district, count ) =>
	axios.get(`/api/v1/push_scenery_cards`, {
		params: { userID, province, city, district, count },
	}).then(res => res.data)

// 获取某卡片具体信息
const getSceneryCard = (cardID) =>
	axios.get(`/api/v1/scenery_card`, {
		params: { cardID, }
	}).then(res => res.data)

// 获取卡片列表（包含简略信息，包括：avatar, username, title, likes, cover）
const getSceneryCards = (cardIDs) =>
	axios.get(`/api/v1/scenery_cards`, {
		params: { cardIDs, },
		paramsSerializer: params => {
			return qs.stringify(params, { indices: false })
		}
	}).then(res => res.data)

// 获取某用户的某些卡片的列表（包含简略信息）
const getSceneryCardsByUser = (userID, type, offset, limit) =>
	axios.get(`/api/v1/scenery_cards_by_user`, {
		params: { userID, type, offset, limit, }
	}).then(res => res.data)

// 获取某景点的卡片列表（包含简略信息）
const getSceneryCardsByScenery = (sceneryID, offset, limit) =>
	axios.get(`/api/v1/scenery_cards_by_scenery`, {
		params: { sceneryID, offset, limit, }
	}).then(res => res.data)

// 点赞/收藏某卡片
const likeSceneryCard = (userID, cardID, cardOwnerID, type) =>
	axios.post(`/api/v1/like_scenery_card`, {
		userID: userID,
		cardID: cardID,
		cardOwnerID: cardOwnerID,
		type: type,
	}).then(res => res.data)

// 检查是否已点赞/收藏某卡片
const checkLikeSceneryCard = (userID, cardID, type) =>
	axios.post(`/api/v1/check_like_scenery_card`, {
		userID: userID,
		cardID: cardID,
		type: type,
	}).then(res => res.data)

// 发表卡片
const postSceneryCard = (sceneryID, userID, title, content, score, pics) =>
	axios.post(`/api/v1/scenery_card`, {
		sceneryID: sceneryID,
		userID: userID,
		title: title,
		content: content,
		score: score,
		pics: pics,
	}).then(res => res.data)


export {
	pushSceneryCards,
	getSceneryCard, getSceneryCards,
	getSceneryCardsByUser,
	getSceneryCardsByScenery,
	likeSceneryCard, checkLikeSceneryCard,
	postSceneryCard,
}
