import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
	{ path:'/', redirect:'/home'},
	{
		path: '/home',
		name: 'Home',
		component: () => import('@/views/Home.vue'),
	},
	{
		path:'/login',
		name:'Login',
		component: () => import('@/views/Login.vue'),
		meta: {
			showNavMenu: false
		}
	},
	{
		path:'/post',
		name:'Post',
		component: () => import('@/views/Post.vue'),
		meta: {
			requireLogin: true,
		}
	},
	{
		path: '/user',
		name: 'User',
		component: () => import('../views/User.vue'),
	},
	{
		path: '/card',
		name: 'Card',
		component: () => import('../views/Card.vue'),
	},
	{
		path: '/scenery',
		name: 'Scenery',
		component: () => import('../views/Scenery.vue'),
	},
	{
		path: '/search/scenery',
		name: 'SearchScenery',
		component: () => import('../views/SearchScenery.vue'),
	},
	{
		path: '/scenery/create',
		name: 'SceneryCreate',
		component: () => import('../views/SceneryCreate.vue'),
		meta: {
			requireLogin: true,
		}
	},
	// {
	// 	path: '/search/tag',
	// 	name: 'SearchTag',
	// 	component: () => import('../views/SearchScenery.vue'),
	// },
	{
		path: '/404',
		name: 'NotFound',
		component: () => import('@/views/NotFound.vue')
	}
]

const router = createRouter({
	history: createWebHashHistory(),
	routes
})

export default router
