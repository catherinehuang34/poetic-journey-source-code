module.exports = {
	devServer: {
		// 以下两个配置不能出现在此处，会有 ValidationError: webpack Dev Server Invalid Options
		// 静态资源文件夹
		// assetsSubDirectory: 'static',
		// 发布路径
		// assetsPublicPath: '/',
		// proxy: null, // string | Object
		proxy: {
			// 设置后，从接口的requestUrl是看不出来的
			"/api": {
				target: "http://localhost:8080", // 会将api字符串替换为target

				// 是否跨域。会开启代理，在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样服务端和服务端进行数据的交互就不会有跨域问题
				changeOrigin: true,

				// 代理 web sockets
				ws: true,

				// pathRewrite: {
				// 	'^/api': '/api' // 将/api替换为后者
				// }
			},
		},
		open: process.platform === 'darwin',
		host: '0.0.0.0',
		port: 8000,
		https: false,
		hotOnly: false,
		before: app => {},
	},
}
// 例子：
// 如果要用接口 http://abc.com/api/test，在本地用 localhost:8080/api/test，如axios.get('/api/test')，设置 target: 'http://abc.com' 后，会做如下转发：
// localhost:8080/api/test -> http://abc.com/api/test
// localhost:8080/qwq/api/test -> http://abc.com/qwq/api/test