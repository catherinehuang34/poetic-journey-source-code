# README

## 环境配置

前端：jsnode+vue

后端：go

## 编译

###### 前端

+ npm instal
+ npm run serve

###### 后端

+ .\main.exe



本项目租用云数据库

## 数据库测试

#### sample level

+ 请在本地运行poetic_journey_create.sql创建数据库
+ 接着执行sample_data插入样本数据

#### production level

+ 打开Navicat Premium，新建一个 ’腾讯云 云数据库 MySQL‘ 连接。

1. Connection Name: TD
2. Host: sh-cynosdbmysql-grp-l7dozfme.sql.tencentcdb.com
3. Port: 29815
4. User name: root
5. Password: PoeticJourney()~

+ 连接中的**poetic_journey_for_test**为项目生产级别的数据库，数据库中可见所有表、数据集。

+ 如需本地测试，请避免在腾讯云数据库上直接运行测试sql语句（插入删除更新语句会影响数据库），请将数据库同步到本地即可测试。
