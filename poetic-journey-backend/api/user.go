package api

import (
	"poetic-journey/service"
	"poetic-journey/util/logging"

	"github.com/gin-gonic/gin"
)

// 更新用户信息
func UserInfoUpdate(c *gin.Context) {
	var service service.UserInfoUpdateService
	if err := c.ShouldBind(&service); err == nil {
		res := service.UpdateUserInfo()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}

// 获取用户其它信息
func UserInfoGet(c *gin.Context) {
	var service service.UserInfoGetService
	if err := c.ShouldBind(&service); err == nil {
		res := service.GetUserInfo()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}

// 获取多个用户的其它信息
func UsersInfoGet(c *gin.Context) {
	var service service.UsersInfoGetService
	if err := c.ShouldBind(&service); err == nil {
		res := service.GetUsersInfo()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}

// 获取用户的社交信息
func UserSocialInfoGet(c *gin.Context) {
	var service service.UserSocialInfoGetService
	if err := c.ShouldBind(&service); err == nil {
		res := service.GetUserSocialInfo()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}
