package api

import (
	"poetic-journey/service"
	"poetic-journey/util/logging"

	"github.com/gin-gonic/gin"
)

// 获取某卡片的评论列表
func GetComments(c *gin.Context) {
	var service service.GetCommentsService
	if err := c.ShouldBind(&service); err == nil {
		res := service.GetComments()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}

// 评论卡片
func PostComment(c *gin.Context) {
	var service service.PostCommentService
	if err := c.ShouldBindJSON(&service); err == nil {
		res := service.PostComment()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}
