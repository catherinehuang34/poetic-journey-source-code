package api

import (
	"poetic-journey/service"
	"poetic-journey/util/logging"

	"github.com/gin-gonic/gin"
)

func PostSceneryCard(c *gin.Context) {
	var service service.PostSceneryCardService
	if err := c.ShouldBind(&service); err == nil {
		res := service.PostSceneryCard()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}

func PushSceneryCards(c *gin.Context) {
	var service service.PushSceneryCardsService
	if err := c.ShouldBind(&service); err == nil {
		res := service.PushSceneryCards()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}

func GetSceneryCard(c *gin.Context) {
	var service service.GetSceneryCardService
	if err := c.ShouldBind(&service); err == nil {
		res := service.GetSceneryCard()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}

func GetSceneryCards(c *gin.Context) {
	var service service.GetSceneryCardsService
	if err := c.ShouldBind(&service); err == nil {
		res := service.GetSceneryCards()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}

func GetSceneryCardsByUser(c *gin.Context) {
	var service service.GetSceneryCardsByUserService
	if err := c.ShouldBind(&service); err == nil {
		res := service.GetSceneryCardsByUser()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}

func GetSceneryCardsByScenery(c *gin.Context) {
	var service service.GetSceneryCardsBySceneryService
	if err := c.ShouldBind(&service); err == nil {
		res := service.GetSceneryCardsByScenery()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}
