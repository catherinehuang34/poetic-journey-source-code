package api

import (
	"poetic-journey/service"
	"poetic-journey/util/logging"

	"github.com/gin-gonic/gin"
)

// 获取用户关注列表
func FollowListGet(c *gin.Context) {
	var service service.FollowListGetService
	if err := c.ShouldBind(&service); err == nil {
		res := service.GetFollowList()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}

// 关注/取关某用户
func FollowUser(c *gin.Context) {
	var service service.FollowUserService
	if err := c.ShouldBindJSON(&service); err == nil {
		res := service.FollowUser()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}

// 是否关注某(些)用户
func FollowUserCheck(c *gin.Context) {
	var service service.FollowUserCheckService
	if err := c.ShouldBind(&service); err == nil {
		res := service.CheckFollowUser()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}
