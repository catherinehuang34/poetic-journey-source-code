package api

import (
	"poetic-journey/service"
	"poetic-journey/util/logging"

	"github.com/gin-gonic/gin"
)

func UploadImage(c *gin.Context) {
	var service service.UploadService
	if err := c.ShouldBind(&service); err == nil {
		res := service.UploadImage()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}
