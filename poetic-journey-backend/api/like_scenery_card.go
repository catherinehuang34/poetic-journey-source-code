package api

import (
	"poetic-journey/service"
	"poetic-journey/util/logging"

	"github.com/gin-gonic/gin"
)

func LikeSceneryCard(c *gin.Context) {
	var service service.LikeSceneryCardService
	if err := c.ShouldBind(&service); err == nil {
		res := service.LikeSceneryCard()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}

func CheckLikeSceneryCard(c *gin.Context) {
	var service service.CheckLikeSceneryCardService
	if err := c.ShouldBind(&service); err == nil {
		res := service.CheckLikeSceneryCard()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}
