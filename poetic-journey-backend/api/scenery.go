package api

import (
	"poetic-journey/service"
	"poetic-journey/util/logging"

	"github.com/gin-gonic/gin"
)

// 用户新建景点
func CreateScenery(c *gin.Context) {
	var service service.CreateSceneryService
	if err := c.ShouldBind(&service); err == nil {
		res := service.CreateScenery()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}

// 获取景点详细信息
func GetSceneryByID(c *gin.Context) {
	var service service.SceneryGetByIDService
	if err := c.ShouldBind(&service); err == nil {
		res := service.GetSceneryByID()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}

// 获取景点列表
func GetSceneryByDetail(c *gin.Context) {
	var service service.SceneryGetByDetailService
	if err := c.ShouldBind(&service); err == nil {
		res := service.GetSceneryByDetail()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}

// 获取景点列表
func GetSceneryByTag(c *gin.Context) {
	var service service.SceneryGetByTagService
	if err := c.ShouldBind(&service); err == nil {
		res := service.GetSceneryByTag()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}

// 获取景点名称列表（以及景点ID）
func GetSceneryNameByDetail(c *gin.Context) {
	var service service.SceneryGetByDetailService
	if err := c.ShouldBind(&service); err == nil {
		res := service.GetSceneryNameByDetail()
		c.JSON(200, res)
	} else {
		logging.Info(err)
		c.JSON(200, ErrorResponse(err))
	}
}
