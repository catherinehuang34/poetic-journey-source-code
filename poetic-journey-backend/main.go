package main

import (
	"poetic-journey/config"
	"poetic-journey/router"
)

func main() {
	config.Init()
	r := router.NewRouter()
	r.Run(":8080")
}
