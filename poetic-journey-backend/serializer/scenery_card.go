package serializer

import (
	"poetic-journey/model"
	"time"
)

// 卡片信息
type SceneryCard struct {
	CardID    int `json:"cardID"`
	SceneryID int `json:"sceneryID"`
	UserID    int `json:"userID"`

	Username    string `json:"username"`
	Avatar      string `json:"avatar"`
	SceneryName string `json:"sceneryName"`

	Title   string `json:"title"`
	Content string `json:"content"`

	Score      float32   `json:"score"`
	Likes      int       `json:"likes"`
	Bookmarks  int       `json:"bookmarks"`
	CreateTime time.Time `json:"createTime"`

	Pictures []string `json:"pictures"`
}

// 卡片简略信息
type SceneryCardBrief struct {
	CardID int `json:"cardID"`
	UserID int `json:"userID"`

	Username string `json:"username"`
	Avatar   string `json:"avatar"`

	Title string `json:"title"`
	Likes int    `json:"likes"`
	Cover string `json:"cover"`
}

//
func BuildSceneryCard(item model.SceneryCard, username, avatar, sceneryName string, pictures []string) SceneryCard {
	return SceneryCard{
		CardID:    item.CardID,
		SceneryID: item.SceneryID,
		UserID:    item.UserID,

		Username:    username,
		Avatar:      avatar,
		SceneryName: sceneryName,

		Title:   item.Title,
		Content: item.Content,

		Score:      item.Score,
		Likes:      item.Likes,
		Bookmarks:  item.Bookmarks,
		CreateTime: item.CreateTime,

		Pictures: pictures,
	}
}

//
// func BuildSceneryCardBrief(item model.SceneryCard, username, avatar string) SceneryCardBrief {
// 	return SceneryCardBrief{
// 		CardID: item.CardID,

// 		Username: username,
// 		Avatar:   avatar,

// 		Title: item.Title,
// 		Likes: item.Likes,
// 		Cover: item.Cover,
// 	}
// }

// 序列化用户信息
// func BuildSceneryCardsBrief(cards []model.SceneryCard, username, avatar []string) []SceneryCardBrief {
// 	var res []SceneryCardBrief

// 	for i, v := range cards {
// 		res = append(res, BuildSceneryCardBrief(v, username[i], avatar[i]))
// 	}

// 	return res
// }
