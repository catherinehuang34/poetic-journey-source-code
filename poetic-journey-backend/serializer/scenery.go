package serializer

import "poetic-journey/model"

// 景点具体信息
type Scenery struct {
	SceneryID   int    `json:"sceneryID"`
	SceneryName string `json:"sceneryName"`

	Province string `json:"province"`
	City     string `json:"city"`
	District string `json:"district"`
	Street   string `json:"street"`

	Intro    string  `json:"intro"`
	OpenTime string  `json:"openTime"`
	Level    string  `json:"level"`
	Score    float32 `json:"score"`
	Price    float32 `json:"price"`

	Pictures []string `json:"pictures"`
}

// 景点在列表中展示的信息
type SceneryBrief struct {
	SceneryID   int    `json:"sceneryID"`
	SceneryName string `json:"sceneryName"`

	Province string `json:"province"`
	City     string `json:"city"`
	District string `json:"district"`
	Street   string `json:"street"`

	Level string  `json:"level"`
	Score float32 `json:"score"`
	Price float32 `json:"price"`

	Cover string `json:"cover"`
}

// 景点名称（以及景点ID）
type SceneryName struct {
	SceneryID   int    `json:"sceneryID"`
	SceneryName string `json:"sceneryName"`
}

// 景点具体信息
func BuildScenery(item model.Scenery, pictures []string) Scenery {
	return Scenery{
		SceneryID:   item.SceneryID,
		SceneryName: item.SceneryName,

		Province: item.Province,
		City:     item.City,
		District: item.District,
		Street:   item.Street,

		Intro:    item.Intro,
		OpenTime: item.OpenTime,
		Level:    item.Level,
		// ! 注意不要除0！
		Score: item.ScoreSum / float32(max(1, item.ScoreCount)),
		Price: item.Price,

		Pictures: pictures,
	}
}

// func BuildSceneries(items []model.Scenery) (sceneries []Scenery) {
// 	for _, item := range items {
// 		sceneries = append(sceneries, BuildScenery(item))
// 	}
// 	return sceneries
// }

// 景点在列表中展示的信息（包含封面）
func BuildSceneryBrief(item model.Scenery, cover string) SceneryBrief {
	return SceneryBrief{
		SceneryID:   item.SceneryID,
		SceneryName: item.SceneryName,

		Province: item.Province,
		City:     item.City,
		District: item.District,
		Street:   item.Street,

		Level: item.Level,
		Score: item.ScoreSum / float32(max(1, item.ScoreCount)),
		Price: item.Price,

		Cover: cover,
	}
}

func BuildSceneriesBrief(items []model.Scenery, covers []string) (sceneries []SceneryBrief) {
	for i, item := range items {
		sceneries = append(sceneries, BuildSceneryBrief(item, covers[i]))
	}
	return sceneries
}

// 景点名称（以及景点ID）
func BuildSceneryName(item model.Scenery) SceneryName {
	return SceneryName{
		SceneryID:   item.SceneryID,
		SceneryName: item.SceneryName,
	}
}

func BuildSceneriesName(items []model.Scenery) (sceneryNames []SceneryName) {
	for _, item := range items {
		sceneryNames = append(sceneryNames, BuildSceneryName(item))
	}
	return sceneryNames
}

func max(args1, args2 int) int {
	if args1 > args2 {
		return args1
	}
	return args2
}
