package serializer

import "poetic-journey/model"

// 用户其它信息
type UserInfo struct {
	UserID   int    `json:"id"`
	Username string `json:"username"`
	Avatar   string `json:"avatar"`

	Gender   string `json:"gender"`
	Age      int    `json:"age"`
	Province string `json:"province"`
	City     string `json:"city"`
	District string `json:"district"`
	Brief    string `json:"brief"`

	Likes     int `json:"likes"`
	Followers int `json:"followers"`
	Followees int `json:"followees"`
}

// （不使用）用户社交信息
type UserSocialInfo struct {
	Likes     int `json:"likes"`
	Followers int `json:"followers"`
	Followees int `json:"followees"`
}

// 序列化用户信息
func BuildUserInfo(item model.User) UserInfo {
	return UserInfo{
		UserID:   item.UserID,
		Username: item.Username,
		Avatar:   item.Avatar,

		Gender:   item.Gender,
		Age:      item.Age,
		Province: item.Province,
		City:     item.City,
		District: item.District,
		Brief:    item.Brief,

		Likes:     item.Likes,
		Followers: item.Followers,
		Followees: item.Followees,
	}
}

// 序列化用户信息
func BuildUsersInfo(users []model.User) []UserInfo {
	var res []UserInfo

	for _, v := range users {
		res = append(res, BuildUserInfo(v))
	}

	return res
}

// 序列化用户社交信息
func BuildUserSocialInfo(likes, followers, followees int) UserSocialInfo {
	return UserSocialInfo{
		likes, followers, followees,
	}
}
