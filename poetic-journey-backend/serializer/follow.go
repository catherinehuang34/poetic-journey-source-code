package serializer

// 关注信息
type Follow struct {
	FollowerID int   `json:"followerID"`
	FolloweeID int   `json:"followeeID"`
	FollowTime int64 `json:"followTime"`
}

// 是否关注
// 直接返回 Data: followed []bool
