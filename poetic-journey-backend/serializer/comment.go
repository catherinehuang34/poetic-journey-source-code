package serializer

import "time"

// 评论信息
type Comment struct {
	CardID    int `json:"cardID"`
	CommentID int `json:"commentID"`
	UserID    int `json:"userID"`

	CommentText string    `json:"commentText"`
	CommentTime time.Time `json:"commentTime"`

	Username string `json:"username"`
	Avatar   string `json:"avatar"`
}
