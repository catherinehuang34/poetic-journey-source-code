package serializer

import (
	"poetic-journey/model"
)

// 用户状态序列化器。将模型转为前端存储的结构
// 通过 json 标记数据
type UserStatus struct {
	ID        int    `json:"id"`
	CreatedAt int64  `json:"createdAt"`
	Username  string `json:"username"`
	Avatar    string `json:"avatar"`

	Status    int `json:"status"`
	Authority int `json:"authority"` // 用户权限，0：管理员，1：普通用户
}

// 序列化用户状态。将模型转为前端存储的结构
func BuildUserStatus(user model.UserStatus, username, avatar string) UserStatus {
	return UserStatus{
		ID:        user.UserID,
		CreatedAt: user.CreatedAt.Unix(),
		Username:  username,
		Avatar:    avatar, // avatar URL 的生成需在service进行。因为serializer不能再引用service。

		Status:    user.Status,
		Authority: user.Authority,
	}
}
