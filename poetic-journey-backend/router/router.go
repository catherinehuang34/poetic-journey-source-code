package router

import (
	"os"
	"poetic-journey/api"
	"poetic-journey/middleware"

	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
)

func NewRouter() *gin.Engine {
	r := gin.Default()

	store := cookie.NewStore([]byte(os.Getenv("SESSION_SECRET")))
	// store.Options(sessions.Options{HttpOnly: true, MaxAge: 7 * 86400, Path: "/"})
	r.Use(sessions.Sessions("mysession", store))

	r.Use(Cors())

	// -----
	// 带 Get, Check 的使用 GET params
	// -----

	// 通用操作
	v0 := r.Group("api/v0")
	{
		// 验证码
		v0.GET("get_captcha", api.GetCaptcha)
		v0.GET("captcha/:captchaID", api.GetCaptchaImage)
	}

	// 用户操作
	v1 := r.Group("/api/v1")
	{
		// 用户注册与登录
		v1.POST("register", api.UserRegister)
		v1.POST("login", api.UserLogin)

		// 景点查询
		v1.GET("scenery_by_detail", api.GetSceneryByDetail)
		v1.GET("scenery_by_tag", api.GetSceneryByTag)
		v1.GET("scenery_name_by_detail", api.GetSceneryNameByDetail)
		v1.GET("scenery_by_id", api.GetSceneryByID)

		// 用户信息查询
		v1.GET("user_info", api.UserInfoGet)
		v1.GET("users_info", api.UsersInfoGet)
		// 社交信息查询，包含：获得的点赞数，粉丝数，关注数（待添加：发表卡片数）
		// v1.GET("user_social_info", api.UserSocialInfoGet)

		// 关注列表
		v1.GET("follow_list", api.FollowListGet)

		// 卡片信息查询
		v1.GET("scenery_card", api.GetSceneryCard)
		v1.GET("scenery_cards", api.GetSceneryCards)
		v1.GET("scenery_cards_by_user", api.GetSceneryCardsByUser)
		v1.GET("scenery_cards_by_scenery", api.GetSceneryCardsByScenery)

		// 获取推送的卡片
		v1.GET("push_scenery_cards", api.PushSceneryCards)

		// 卡片评论列表
		v1.GET("comments", api.GetComments)

		// 登录信息验证
		auth := v1.Group("/")
		auth.Use(middleware.JWT())
		{
			// 验证token（用中间件）
			auth.GET("check_token", api.Ping)

			// 更新个人信息
			auth.POST("user_info", api.UserInfoUpdate)

			// 图片上传
			auth.POST("upload_image", api.UploadImage)

			// 发表卡片
			auth.POST("scenery_card", api.PostSceneryCard)

			// 新建景点
			auth.POST("scenery", api.CreateScenery)

			// 关注/取关
			auth.POST("follow_user", api.FollowUser)
			auth.GET("follow_check", api.FollowUserCheck)

			// 点赞/收藏卡片
			auth.POST("like_scenery_card", api.LikeSceneryCard)
			auth.POST("check_like_scenery_card", api.CheckLikeSceneryCard)

			// 评论卡片
			auth.POST("comment", api.PostComment)
		}
	}

	// v2 := r.Group("/api/v2")
	// {
	// 	auth2 := v2.Group("/")
	// 	auth2.Use(middleware.JWTAdmin())
	// 	{
	// 		// 管理员信息操作
	// 		auth2.GET("info", api.AdminInfoGet)
	// 	}
	// }

	// sdk
	v3 := r.Group("/api/v3")
	{
		// 获取下级行政区列表
		v3.GET("amap/districts", api.GetDistricts)
	}

	// 404 NotFound
	// r.NoRoute(func(c *gin.Context) {
	// 	c.JSON(http.StatusNotFound, gin.H{
	// 		"status": 404,
	// 		"error": "404, page not found",
	// 	})
	// })

	return r
}

// Cors 跨域配置
func Cors() gin.HandlerFunc {
	config := cors.DefaultConfig()
	// 访问跨域请求的http方法
	config.AllowMethods = []string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD", "OPTIONS"}
	// 允许跨域请求的头
	config.AllowHeaders = []string{"Origin", "Content-Length", "Content-Type", "Cookie", "Authorization"}
	// 允许跨域请求的源
	config.AllowOrigins = []string{"http://localhost:8080", "http://localhost:8000", "http://localhost:8198", "http://localhost:8081"}
	config.AllowCredentials = true
	return cors.New(config)
}
