package model

import "time"

type Comment struct {
	CardID    int `gorm:"unique; primaryKey"`
	CommentID int
	UserID    int

	CommentText string
	CommentTime time.Time
}
