package model

import "time"

type SceneryCard struct {
	CardID    int `gorm:"unique; primaryKey"`
	SceneryID int
	UserID    int

	Title   string
	Content string

	Score      float32
	Likes      int
	Bookmarks  int
	CreateTime time.Time

	// Cover string // 暂时没有使用
}
