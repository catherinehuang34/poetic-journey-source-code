package model

type User struct {
	UserID   int    `gorm:"unique; primaryKey"`
	Username string `gorm:"unique"`
	Avatar   string

	Gender   string
	Age      int
	Province string
	City     string
	District string

	Brief string

	Likes     int
	Followers int
	Followees int
}
