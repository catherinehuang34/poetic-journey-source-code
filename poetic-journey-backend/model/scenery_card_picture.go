package model

type SceneryCardPicture struct {
	CardID int
	PicID  int
	Pic    string
}
