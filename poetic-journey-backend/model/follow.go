package model

import "time"

type Follow struct {
	FollowerID int
	FolloweeID int
	FollowTime time.Time
}
