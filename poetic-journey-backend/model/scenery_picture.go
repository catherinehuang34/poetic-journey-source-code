package model

type SceneryPicture struct {
	SceneryID int `gorm:"unique"`
	PicID     int
	Pic       string // url or key
}
