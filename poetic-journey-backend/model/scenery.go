package model

// scenery (scenery_id, scenery_name, province, city, district, street, intro, open_time, level, score, price)
type Scenery struct {
	SceneryID   int `gorm:"unique; primaryKey"`
	SceneryName string

	Province string
	City     string
	District string
	Street   string

	Intro    string
	OpenTime string
	Level    string
	Price    float32

	// Score    float32
	ScoreSum   float32
	ScoreCount int
}
