package service

import (
	"poetic-journey/constant"
	"poetic-journey/model"
	"poetic-journey/serializer"
	"poetic-journey/util/logging"
	"time"
)

// 发表卡片
type PostSceneryCardService struct {
	SceneryID int `json:"sceneryID" binding:"required"`
	UserID    int `json:"userID" binding:"required"`

	Title   string `json:"title" binding:"required"`
	Content string `json:"content"`

	Score float32 `json:"score" binding:"required"`

	Pics []string `json:"pics" binding:"required"`
}

// 发表卡片
// 返回: 新建后的卡片id
func (service *PostSceneryCardService) PostSceneryCard() serializer.Response {
	code := constant.SUCCESS

	err := model.DB.Exec(
		`insert into scenery_card(scenery_id, user_id, title, content, score, likes, bookmarks, create_time)
			values(?, ?, ?, ?, ?, ?, ?, ?)`, service.SceneryID, service.UserID, service.Title, service.Content, service.Score, 0, 0, time.Now()).Error

	// 获取cardID
	cardID := 0
	if err == nil {
		err = model.DB.Raw(
			`select card_id from scenery_card
				where scenery_id = ? and user_id = ? and title = ?`, service.SceneryID, service.UserID, service.Title).Scan(&cardID).Error
	}

	// 更新图片
	for i, v := range service.Pics {
		if err != nil {
			break
		}
		err = model.DB.Exec(
			`insert into scenery_card_picture(card_id, pic_id, pic)
				values(?, ?, ?)`, cardID, i+1, v).Error
	}

	// 更新评分
	if err == nil {
		err = model.DB.Exec(
			`update scenery set score_count = score_count + 1, score_sum = score_sum + ?
				where scenery_id = ?`, service.Score, service.SceneryID).Error
	}

	if err != nil {
		// 数据库错误
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   cardID,
	}
}
