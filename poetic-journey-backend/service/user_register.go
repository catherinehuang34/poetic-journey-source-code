package service

import (
	"poetic-journey/constant"
	"poetic-journey/model"
	"poetic-journey/serializer"
	"poetic-journey/util/logging"
)

// 管理用户注册服务的结构
type UserRegisterService struct {
	Username  string `json:"username" binding:"required"`
	Password  string `json:"password" binding:"required"`
	Captcha   string `json:"captcha" binding:"required"`
	CaptchaID string `json:"captchaID" binding:"required"`
}

// 验证信息
func (service *UserRegisterService) Validate() *serializer.Response {
	var count int64
	count = 0
	code := constant.SUCCESS

	// 在User里判断Username是否重复。User里旧数据不能忽略。
	// err := model.DB.Model(&model.UserStatus{}).Where("username = ?", service.Username).Count(&count).Error
	err := model.DB.Raw(
		`select count(*) from user
		where username = ?`,
		service.Username).Scan(&count).Error

	if err != nil {
		code = constant.ERROR_DATABASE
		return &serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}
	if count > 0 {
		code = constant.ERROR_EXISTED_USER
		return &serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}
	return nil
}

// 用户注册
func (service *UserRegisterService) RegisterUser() serializer.Response {
	userStatus := model.UserStatus{
		// Username:  service.Username,
		Status:    model.StatusNormal,
		Authority: model.AuthorityUser,
	}
	code := constant.SUCCESS

	// 先进行验证码验证
	if code = VerifyCaptcha(service.Captcha, service.CaptchaID); code != constant.SUCCESS {
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}

	// 信息验证
	if res := service.Validate(); res != nil {
		return *res
	}

	// 加密密码
	if err := userStatus.SetPassword(service.Password); err != nil {
		logging.Info(err)
		code = constant.ERROR_FAIL_ENCRYPTION
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}

	// 创建用户元数据
	if err := model.DB.Model(&model.UserStatus{}).Create(&userStatus).Error; err != nil {
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}

	userID := int(userStatus.ID) + 100000

	// 创建用户其它信息
	info := UserInfoCreateService{
		Username: service.Username,
	}
	if err := info.CreateUserInfo(&userID); err != nil {
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}

	// 更新用户元数据的UserID
	if err := model.DB.Model(&model.UserStatus{}).Where("ID = ?", userStatus.ID).Update("UserID", userID).Error; err != nil {
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
	}
}
