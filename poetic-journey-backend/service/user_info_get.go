package service

import (
	"errors"
	"poetic-journey/constant"
	"poetic-journey/model"
	"poetic-journey/serializer"
	"poetic-journey/util/logging"

	"gorm.io/gorm"
)

// 获取用户其它信息
type UserInfoGetService struct {
	UserID int `form:"userID" binding:"required"` // ! get params使用form 而非json
}

type UsersInfoGetService struct {
	UserIDs []int `form:"userIDs" binding:"required"`
}

// 获取用户社交信息
type UserSocialInfoGetService struct {
	UserID int `form:"userID" binding:"required"`
}

// 获取用户其它信息
func (service *UserInfoGetService) GetUserInfo() serializer.Response {
	var user model.User
	code := constant.SUCCESS

	err := model.DB.Model(&model.User{}).Where("user_id = ?", service.UserID).Find(&user).Error
	if err != nil {
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}
	user.Avatar = GenerateAvatar(user.Avatar, service.UserID)

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   serializer.BuildUserInfo(user),
	}
}

// 获取多个用户的其它信息
func (service *UsersInfoGetService) GetUsersInfo() serializer.Response {
	// fmt.Println(service.UserIDs)
	var user model.User
	var users []model.User
	code := constant.SUCCESS

	for _, v := range service.UserIDs {
		err := model.DB.Raw(`select * from user where user_id = ?`, v).Scan(&user).Error
		if err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				continue
			}

			logging.Info(err)
			code = constant.ERROR_DATABASE
			return serializer.Response{
				Status: code,
				Msg:    constant.GetMsg(code),
				Error:  err.Error(),
			}
		}
		user.Avatar = GenerateAvatar(user.Avatar, v)
		users = append(users, user)
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   serializer.BuildUsersInfo(users),
	}
}

// （目前不使用）获取用户社交信息。包含：获得的点赞数，粉丝数，关注数（待添加：发表卡片数）
func (service *UserSocialInfoGetService) GetUserSocialInfo() serializer.Response {
	var likesArr []int
	var likes, followers, followees int
	code := constant.SUCCESS

	// 获得的点赞数
	err := model.DB.Raw(
		"select likes from `like` join `scenery_card` using (card_id) "+
			"where scenery_card.user_id = ?",
		service.UserID).Scan(&likesArr).Error
	if err == nil {
		for _, v := range likesArr {
			likes += v
		}
	}

	// 粉丝数
	if err == nil {
		err = model.DB.Raw(
			`select count(*) from follow
			where followee_id = ?`,
			service.UserID).Scan(&followers).Error
	}

	// 关注数
	if err == nil {
		err = model.DB.Raw(
			`select count(*) from follow
			where follower_id = ?`,
			service.UserID).Scan(&followees).Error
	}

	if err != nil {
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   serializer.BuildUserSocialInfo(likes, followers, followees),
	}
}
