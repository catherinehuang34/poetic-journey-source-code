package service

import (
	"poetic-journey/constant"
	"poetic-journey/model"
	"poetic-journey/serializer"
	"poetic-journey/util/logging"
)

// 修改用户信息（性别、年龄、省市区、简介、头像）
type UserInfoUpdateService struct {
	UserID int    `json:"userID" binding:"required"`
	Gender string `json:"gender" binding:"required"`
	Age    int    `json:"age"`

	Province string `json:"province"`
	City     string `json:"city"`
	District string `json:"district"`

	Brief  string `json:"brief"`
	Avatar string `json:"avatar"` // an OSS key
}

// 修改用户信息（性别、年龄、省市区、简介、头像）。前端传入时需保证 用户不修改则为原始值。
func (service *UserInfoUpdateService) UpdateUserInfo() serializer.Response {
	service.check()

	var user model.User
	code := constant.SUCCESS

	err := model.DB.Raw("select * from user where user_id = ?", service.UserID).Scan(&user).Error

	if err == nil {
		user.Gender = service.Gender
		if service.Age > 0 {
			user.Age = service.Age
		}

		if !IsEmptyAddress(service.Province) {
			user.Province = service.Province
			if !IsEmptyAddress(service.City) {
				user.City = service.City
				if !IsEmptyAddress(service.District) {
					user.District = service.District
				}
			}
		}

		user.Brief = service.Brief
		if service.Avatar != "" {
			user.Avatar = service.Avatar
		}

		err = model.DB.Model(&model.User{}).Where("user_id = ?", user.UserID).Save(&user).Error
	}

	if err != nil {
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   serializer.BuildUserInfo(user),
	}
}

func (service *UserInfoUpdateService) check() {
	if service.Province == "香港特别行政区" {
		service.Province = "香港"
	}
	if service.City == "上海城区" {
		service.City = "上海市"
	}
	if service.City == "北京城区" {
		service.City = "北京市"
	}
}
