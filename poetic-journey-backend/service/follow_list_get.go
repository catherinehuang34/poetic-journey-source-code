package service

import (
	"poetic-journey/constant"
	"poetic-journey/model"
	"poetic-journey/serializer"
	"poetic-journey/util/logging"
)

// 获取用户关注/粉丝列表
type FollowListGetService struct {
	UserID int  `form:"userID" binding:"required"`
	Type   *int `form:"type" binding:"required"` // 0:follower 1:followee // 需使用*int 传入required的零值
	Offset int  `form:"offset"`
	Limit  int  `form:"limit"`
}

// 获取用户关注/粉丝列表
// 仅获取id，需前端再进行其它信息的获取。
// 返回：IDs []int
func (service *FollowListGetService) GetFollowList() serializer.Response {
	code := constant.SUCCESS

	service.checkLimit()

	var queryStr string
	if *service.Type == 0 {
		queryStr =
			`select follower_id FROM follow
			where followee_id = ?
			order by follow_time DESC
			limit ?, ?`
	} else {
		queryStr =
			`select followee_id FROM follow
			where follower_id = ?
			order by follow_time DESC
			limit ?, ?`
	}

	// ! 注意 var IDs []int 在没有元素（不会进行append）时会返回一个null，而不是空数组！
	IDs := make([]int, 0)
	err := model.DB.Raw(
		queryStr, service.UserID, service.Offset, service.Limit).Scan(&IDs).Error

	if err != nil {
		// 数据库错误
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   IDs,
	}
}

func (service *FollowListGetService) checkLimit() {
	if service.Limit == 0 {
		service.Limit = 20
	}
}
