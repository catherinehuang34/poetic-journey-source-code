package service

var avatars = []string{
	"https://www.helloimg.com/images/2022/06/24/Znw7kD.jpg",
	"https://www.helloimg.com/images/2022/06/24/Zn0Zuz.jpg",
	"https://www.helloimg.com/images/2022/06/24/Zn0GLn.jpg",
	"https://www.helloimg.com/images/2022/06/24/Zn0CR6.jpg",
	"https://www.helloimg.com/images/2022/06/24/Zn0RQR.png",
	"https://www.helloimg.com/images/2022/06/24/Zn056P.jpg",
	"https://www.helloimg.com/images/2022/06/24/Zn0ar0.jpg",
	"https://www.helloimg.com/images/2022/06/24/Zn0FJA.jpg",
	"https://www.helloimg.com/images/2022/06/24/Zn0Pc5.jpg",
	"https://www.helloimg.com/images/2022/06/24/Zn033m.png",
}

// 根据key或网址或空串，生成Avatar
// 摆烂了，不返回code, err了。
func GenerateAvatar(avatar string, userID int) string {
	if avatar == "" {
		avatar = avatars[userID%len(avatars)]
	}
	url, _, _ := GenerateURL(avatar)
	return url
}
