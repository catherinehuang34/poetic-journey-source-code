package service

import (
	"database/sql"
	"poetic-journey/constant"
	"poetic-journey/model"
	"poetic-journey/serializer"
	"poetic-journey/util/logging"
)

// 获取某卡片具体信息
type GetSceneryCardService struct {
	CardID int `form:"cardID" binding:"required"`
}

// 获取卡片列表（包含简略信息，包括：avatar, username, title, likes, cover）
type GetSceneryCardsService struct {
	CardIDs []int `form:"cardIDs" binding:"required"`
}

// 获取某用户的某些卡片的列表（包含简略信息）
type GetSceneryCardsByUserService struct {
	UserID int `form:"userID" binding:"required"`
	Type   int `form:"type" binding:"required"` // 1:发表的 2:点赞的 3:收藏的
	Offset int `form:"offset"`
	Limit  int `form:"limit"`
}

// 获取某景点的卡片列表（包含简略信息）
type GetSceneryCardsBySceneryService struct {
	SceneryID int `form:"sceneryID" binding:"required"`
	Offset    int `form:"offset"`
	Limit     int `form:"limit"`
}

// 获取卡片信息
func (service *GetSceneryCardService) GetSceneryCard() serializer.Response {
	var card model.SceneryCard
	var username, avatar, sceneryName string
	var pictures []string
	code := constant.SUCCESS

	err := model.DB.Raw("select * from scenery_card where card_id = ?", service.CardID).Scan(&card).Error

	if err == nil {
		type Result struct {
			Username string // 注意字段名要大写
			Avatar   string
		}
		var res Result
		err = model.DB.Raw(`select username, avatar from user where user_id = ?`, card.UserID).Scan(&res).Error
		username, avatar = res.Username, GenerateAvatar(res.Avatar, card.UserID)
	}

	if err == nil {
		err = model.DB.Raw(`select scenery_name from scenery where scenery_id = ?`, card.SceneryID).Scan(&sceneryName).Error
	}

	if err == nil {
		var temp []sql.NullString
		err = model.DB.Raw(`select pic from scenery_card_picture where card_id = ?`, card.CardID).Scan(&temp).Error

		for _, v := range temp {
			url, code2, err2 := GenerateURL(v.String)
			if err2 != nil {
				code = code2
				err = err2
				break
			}
			pictures = append(pictures, url)
		}
	}

	if err != nil {
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   serializer.BuildSceneryCard(card, username, avatar, sceneryName, pictures),
	}
}

// 根据CardIDs，获取某些卡片的简略信息
// 返回：[]serializer.SceneryCardBrief
func (service *GetSceneryCardsService) GetSceneryCards() serializer.Response {
	data, code, err := getSceneryCards(service.CardIDs)

	if err != nil {
		logging.Info(err)
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   data,
	}
}

// 获取某用户的某些卡片的列表（包含简略信息）
// 返回：[]serializer.SceneryCardBrief
func (service *GetSceneryCardsByUserService) GetSceneryCardsByUser() serializer.Response {
	var cardIDs []int
	var data []serializer.SceneryCardBrief

	code := constant.SUCCESS

	if service.Limit == 0 {
		service.Limit = 12
	}

	var err error
	// 1:发表的 2:点赞的 3:收藏的
	if service.Type == 1 {
		err = model.DB.Raw(`select card_id from scenery_card where user_id = ? order by likes DESC, create_time DESC limit ?, ?`, service.UserID, service.Offset, service.Limit).Scan(&cardIDs).Error
	} else if service.Type == 2 {
		err = model.DB.Raw("select card_id from `like` where user_id = ? limit ?, ?", service.UserID, service.Offset, service.Limit).Scan(&cardIDs).Error
	} else {
		err = model.DB.Raw("select card_id from `bookmark` where user_id = ? limit ?, ?", service.UserID, service.Offset, service.Limit).Scan(&cardIDs).Error
	}

	if err == nil {
		data, code, err = getSceneryCards(cardIDs)
	}

	if err != nil {
		logging.Info(err)
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   data,
	}
}

// 获取某景点的卡片列表（包含简略信息）
// 返回：[]serializer.SceneryCardBrief
func (service *GetSceneryCardsBySceneryService) GetSceneryCardsByScenery() serializer.Response {
	var cardIDs []int
	var data []serializer.SceneryCardBrief

	code := constant.SUCCESS

	if service.Limit == 0 {
		service.Limit = 12
	}

	err := model.DB.Raw(
		`select card_id from scenery_card where scenery_id = ?
		order by likes DESC, create_time DESC
		limit ?, ?`, service.SceneryID, service.Offset, service.Limit).Scan(&cardIDs).Error

	if err == nil {
		data, code, err = getSceneryCards(cardIDs)
	}

	if err != nil {
		logging.Info(err)
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   data,
	}
}

// 获取某些卡片的列表（包含简略信息）
// 返回：[]serializer.SceneryCardBrief
func getSceneryCards(cardIDs []int) ([]serializer.SceneryCardBrief, int, error) {
	type Result struct {
		UserID int
		Title  string
		Likes  int
	}
	type Result2 struct {
		Username string
		Avatar   string
	}

	data := make([]serializer.SceneryCardBrief, 0) // 不要用var

	code := constant.SUCCESS

	for _, v := range cardIDs {
		var res Result
		var res2 Result2
		var cover string

		err := model.DB.Raw("select user_id, title, likes from scenery_card where card_id = ?", v).Scan(&res).Error

		if err == nil {
			var temp sql.NullString
			err = model.DB.Raw("select pic from scenery_card_picture where card_id = ? and pic_id = 1", v).Scan(&temp).Error

			if err == nil {
				url, code2, err2 := GenerateURL(temp.String)
				if err2 != nil {
					err = err2
					code = code2
				}
				cover = url
			}
		}

		if err == nil {
			err = model.DB.Raw(`select username, avatar from user where user_id = ?`, res.UserID).Scan(&res2).Error
			if err == nil {
				res2.Avatar = GenerateAvatar(res2.Avatar, res.UserID)
			}
		}

		if err != nil {
			code = constant.ERROR_DATABASE
			return data, code, err
		}

		data = append(data, serializer.SceneryCardBrief{
			v, res.UserID, res2.Username, res2.Avatar, res.Title, res.Likes, cover,
		})
	}

	return data, code, nil
}
