package service

import (
	"errors"
	"poetic-journey/constant"
	"poetic-journey/model"
	"poetic-journey/serializer"
	"poetic-journey/util"
	"poetic-journey/util/logging"

	"gorm.io/gorm"
)

// 管理用户登录服务的结构
type UserLoginService struct {
	Username  string `json:"username" binding:"required"`
	Password  string `json:"password" binding:"required"`
	Captcha   string `json:"captcha" binding:"required"`
	CaptchaID string `json:"captchaID" binding:"required"`
}

// 用户登录函数
func (service *UserLoginService) LoginUser() serializer.Response {
	var user model.UserStatus
	var userInfo model.User
	code := constant.SUCCESS

	// 先进行验证码验证
	if code = VerifyCaptcha(service.Captcha, service.CaptchaID); code != constant.SUCCESS {
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}

	err := model.DB.Raw(`select * from user where username = ?`, service.Username).Scan(&userInfo).Error
	if err == nil {
		err = model.DB.Model(&model.UserStatus{}).Where("user_id = ?", userInfo.UserID).First(&user).Error
	}

	if err != nil {
		// 无用户记录
		if errors.Is(err, gorm.ErrRecordNotFound) {
			// logging.Info(err)
			code = constant.ERROR_NO_SUCH_USER
			return serializer.Response{
				Status: code,
				Msg:    constant.GetMsg(code),
			}
		}

		// 数据库错误
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}

	if user.CheckPassword(service.Password) == false {
		code = constant.ERROR_WRONG_PASSWORD
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}

	token, err := util.GenerateToken(service.Username, service.Password, user.Status, user.Authority)
	if err != nil {
		logging.Info(err)
		code = constant.ERROR_AUTH_TOKEN
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}
	userInfo.Avatar = GenerateAvatar(userInfo.Avatar, user.UserID)
	return serializer.Response{
		Data:   serializer.TokenData{User: serializer.BuildUserStatus(user, userInfo.Username, userInfo.Avatar), Token: token},
		Status: code,
		Msg:    constant.GetMsg(code),
	}
}
