package service

import (
	"poetic-journey/model"
)

type UserInfoCreateService struct {
	// UserID   int
	Username string
}

// 创建用户其它信息
func (service *UserInfoCreateService) CreateUserInfo(userID *int) error {
	user := model.User{
		// UserID:   service.UserID, // 自增
		Username: service.Username,
		Avatar:   "https://www.helloimg.com/images/2022/06/24/Znw7kD.jpg", // 默认头像

		Gender:   "保密",
		Age:      0,
		Province: "",
		City:     "",
		District: "",

		Brief: "",

		Likes:     0,
		Followers: 0,
		Followees: 0,
	}

	err := model.DB.Model(&model.User{}).Create(&user).Error
	userID = &user.UserID

	return err
}
