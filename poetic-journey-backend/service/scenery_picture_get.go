package service

import (
	"poetic-journey/constant"
	"poetic-journey/model"
)

// 获取景点第一张图片的URL（封面）
func getSceneryCover(sceneryID int) (url string, code int, err error) {
	code = constant.SUCCESS
	var pic model.SceneryPicture

	err = model.DB.Raw(
		`select * from scenery_picture
		where scenery_id = ? and pic_id = 1`,
		sceneryID).Scan(&pic).Error

	if err != nil {
		code = constant.ERROR_DATABASE
		return
	}

	if pic.Pic == "" {
		pic.Pic = NO_IMG
	}

	url, code, err = GenerateURL(pic.Pic)
	return
}

// 获取景点所有图片的URL
func getSceneryPictures(sceneryID int) (urls []string, code int, err error) {
	code = constant.SUCCESS
	var pics []model.SceneryPicture

	err = model.DB.Raw(
		`select * from scenery_picture
		where scenery_id = ?`,
		sceneryID).Scan(&pics).Error
	if err != nil {
		code = constant.ERROR_DATABASE
		return
	}

	if len(pics) == 0 {
		pics = append(pics, model.SceneryPicture{Pic: NO_IMG})
	}
	// 最多返回6张图片
	if len(pics) > 6 {
		pics = pics[0:6]
	}

	for _, item := range pics {
		url, code2, err2 := GenerateURL(item.Pic)
		if err2 != nil {
			err = err2
			code = code2
			break
		}
		urls = append(urls, url)
	}

	return
}
