package service

import (
	"poetic-journey/constant"
	"poetic-journey/model"
	"poetic-journey/serializer"
	"poetic-journey/util/logging"
)

// 点赞/收藏某卡片
type LikeSceneryCardService struct {
	UserID      int `json:"userID" binding:"required"`
	CardID      int `json:"cardID" binding:"required"`
	CardOwnerID int `json:"cardOwnerID" binding:"required"`
	Type        int `json:"type" binding:"required"` // 1:点赞 2:取消点赞 3:收藏 4:取消收藏
}

// 检查是否已点赞/收藏某卡片
type CheckLikeSceneryCardService struct {
	UserID int `json:"userID" binding:"required"`
	CardID int `json:"cardID" binding:"required"`
	Type   int `json:"type" binding:"required"` // 1:点赞 3:收藏
}

// 点赞/收藏某卡片
func (service *LikeSceneryCardService) LikeSceneryCard() serializer.Response {
	// 是否已关注
	liked, code, err := checkLikeSceneryCard(service.UserID, service.CardID, service.Type)
	if err == nil {
		if service.Type == 1 && liked {
			return serializer.Response{
				Status: constant.INVALID_PARAMS,
				Msg:    "已经点赞过该卡片了哦",
			}
		} else if service.Type == 2 && !liked {
			return serializer.Response{
				Status: constant.INVALID_PARAMS,
				Msg:    "还没有喜欢过该卡片哦",
			}
		} else if service.Type == 3 && liked {
			return serializer.Response{
				Status: constant.INVALID_PARAMS,
				Msg:    "已经收藏过该卡片了哦",
			}
		} else if service.Type == 4 && !liked {
			return serializer.Response{
				Status: constant.INVALID_PARAMS,
				Msg:    "还没有收藏过该卡片哦",
			}
		}
	}

	// 功能
	tableName := "`like`"
	if service.Type >= 3 {
		tableName = "`bookmark`"
	}
	if err == nil {
		if service.Type&1 == 1 { // Like/Bookmark
			err = model.DB.Exec(
				"insert into "+tableName+"(user_id, card_id) values(?, ?)",
				service.UserID, service.CardID).Error
		} else { // Cancel like/bookmark
			err = model.DB.Exec(
				"delete from "+tableName+" where user_id = ? and card_id = ?",
				service.UserID, service.CardID).Error
		}

		// update user.like
		val := -1 // cancel
		if service.Type&1 == 1 {
			val = 1 // execute
		}
		if err == nil && service.Type <= 2 {
			err = model.DB.Exec(
				`update user
				set likes = likes + ?
				where user_id = ?`,
				val, service.CardOwnerID).Error
		}

		// update scenery_card.likes/bookmarks
		if err == nil {
			if service.Type <= 2 {
				err = model.DB.Exec(
					`update scenery_card
					set likes = likes + ?
					where card_id = ?`,
					val, service.CardID).Error
			} else {
				err = model.DB.Exec(
					`update scenery_card
					set bookmarks = bookmarks + ?
					where card_id = ?`,
					val, service.CardID).Error
			}
		}
	}

	if err != nil {
		// 数据库错误
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
	}
}

// 检查是否已点赞/收藏某卡片
// 返回：liked bool
func (service *CheckLikeSceneryCardService) CheckLikeSceneryCard() serializer.Response {
	liked, code, err := checkLikeSceneryCard(service.UserID, service.CardID, service.Type)

	if err != nil {
		logging.Info(err)
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   liked,
	}
}

// 检查是否已点赞/收藏某卡片
func checkLikeSceneryCard(userID, cardID, queryType int) (liked bool, code int, err error) {
	var count int
	code = constant.SUCCESS

	tableName := "`like`"
	if queryType >= 3 {
		tableName = "`bookmark`"
	}
	err = model.DB.Raw("select count(*) from "+tableName+" where user_id = ? and card_id = ?", userID, cardID).Scan(&count).Error

	if err != nil {
		code = constant.ERROR_DATABASE
		return
	}

	liked = count > 0
	return
}
