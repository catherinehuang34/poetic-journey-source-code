package service

import (
	"math/rand"
	"poetic-journey/constant"
	"poetic-journey/model"
	"poetic-journey/serializer"
	"poetic-journey/util/logging"
)

// 推送卡片，返回卡片列表（包含简略信息，包括：avatar, username, title, likes, cover）
type PushSceneryCardsService struct {
	UserID   int    `form:"userID"`
	Province string `form:"province"`
	City     string `form:"city"`
	District string `form:"district"`
	Count    int    `form:"count"` // 第几次查询（从0开始）
}

const (
	FollowLimit   = 4 // 关注用户 单次推送数（如果不足，推送附近）
	NeighborLimit = 8 // 附近景点 单次推送数
)

// 根据CardIDs，获取某些卡片的简略信息
// 返回：[]serializer.SceneryCardBrief
func (service *PushSceneryCardsService) PushSceneryCards() serializer.Response {
	var cardIDs1, cardIDs2 []int

	// 关注用户
	// 目前使用随机，但是就不能按时间/点赞排序了
	var err error
	limit := FollowLimit
	if service.UserID > 0 {
		var total int
		model.DB.Raw(
			`select count(*)
			from scenery_card
			where user_id in
				(select followee_id from follow
				where follower_id = ?)`, service.UserID).Scan(&total)

		for ; total-len(cardIDs1) > 5 && limit > 0; limit-- {
			var cardID int
			err = model.DB.Raw(
				`select card_id
				from scenery_card
				where user_id in
					(select followee_id from follow
					where follower_id = ?)
				limit ?, 1`, service.UserID, rand.Intn(total)).Scan(&cardID).Error
			cardIDs1 = append(cardIDs1, cardID)
			// fmt.Println("1:", cardIDs1)
		}
		// err = model.DB.Raw(
		// 	`select card_id
		// 	from scenery_card
		// 	where user_id in
		// 		(select followee_id from follow
		// 		where follower_id = ?)
		// 	order by create_time desc
		// 	limit ?, ?`, service.UserID, service.Count*FollowLimit, FollowLimit).Scan(&cardIDs1).Error
	}

	// 附近
	if err == nil {
		limit = NeighborLimit + limit
		// 相同地区推荐
		// 目前只能有序推荐，卡片不多，随机推荐的话容易重复
		if service.District != "" {
			err = model.DB.Raw(
				`select card_id
				from scenery_card natural join scenery
				where province = ? and city = ? and district = ?
				limit ?, ?`, service.Province, service.City, service.District, service.Count*NeighborLimit, limit).Scan(&cardIDs2).Error
			// fmt.Println("2:", cardIDs2)
		}

		limit = limit - len(cardIDs2)
		if limit > 0 && err == nil {
			// 相同市推荐
			// 随机推荐，因为不方便处理offset，但这样可能重复
			if service.City != "" {
				var total int
				model.DB.Raw(
					`select count(*)
					from scenery_card natural join scenery
					where province = ? and city = ?`, service.Province, service.City).Scan(&total)

				var cardIDs3 []int
				for ; total-len(cardIDs3) > 5 && limit > 0; limit-- {
					var cardID int
					offset := rand.Intn(total)
					err = model.DB.Raw(
						`select card_id
						from scenery_card natural join scenery
						where province = ? and city = ?
						limit ?, 1`, service.Province, service.City, offset).Scan(&cardID).Error
					cardIDs3 = append(cardIDs3, cardID)
				}
				limit = limit - len(cardIDs3)
				cardIDs2 = append(cardIDs2, cardIDs3...)
				// fmt.Println("3:", cardIDs3)
			}

			// 随机推荐
			if limit > 0 && err == nil {
				// fmt.Println("4:", limit)
				var total int
				model.DB.Raw(`select count(*) from scenery_card`).Scan(&total)

				var cardIDs4 []int
				for ; limit > 0; limit-- {
					var cardID int
					offset := rand.Intn(total)
					err = model.DB.Raw(
						// 可以加 where user_id != UserID，但是会增加查询代价（如果用user_id索引，则查出来只能是user_id连续的）
						`select card_id
						from scenery_card
						limit ?, 1`, offset).Scan(&cardID).Error
					cardIDs4 = append(cardIDs4, cardID)
				}
				cardIDs2 = append(cardIDs2, cardIDs4...)
				// fmt.Println("4:", cardIDs4)
			}
		}
	}

	if err != nil {
		logging.Info(err)
		code := constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}

	data, code, err := getSceneryCards(append(cardIDs1, cardIDs2...)) // // 关注放在后面，因为每次刷新都重复...

	if err != nil {
		logging.Info(err)
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   data,
	}
}
