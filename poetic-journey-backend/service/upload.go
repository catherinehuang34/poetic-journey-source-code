package service

import (
	"mime"
	"os"
	"path/filepath"
	"poetic-journey/constant"
	"poetic-journey/serializer"
	"poetic-journey/util/logging"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/google/uuid"
)

// 上传文件
type UploadService struct {
	Filename string `json:"filename"`
}

const NO_IMG = "https://www.helloimg.com/images/2022/06/17/ZSiyJv.png"

// OSS：将key转为可访问的URL
func GenerateURL(key string) (url string, code int, err error) {
	url = NO_IMG
	code = constant.SUCCESS
	if key == "" {
		return
	}

	if len(key) >= 4 && key[0:4] == "http" {
		// 失效图片：
		// https://flight-feed.qunarzz.com/as3/180/image/poi_vishnu/...
		// https://mp-piao-admincp.qunarzz.com/mp_piao_admin_mp_piao_admin/fusion/...
		if len(key) >= 19 && (key[8:14] == "flight" || key[8:15] == "mp-piao") {
			return
		}
		return key, code, nil
	}

	client, err := oss.New(os.Getenv("OSS_END_POINT"), os.Getenv("OSS_ACCESS_KEY_ID"), os.Getenv("OSS_ACCESS_KEY_SECRET"))
	if err != nil {
		code = constant.ERROR_OSS
		return
	}
	bucket, err := client.Bucket(os.Getenv("OSS_BUCKET"))
	if err != nil {
		code = constant.ERROR_OSS
		return
	}

	// 查看图片
	signedGetURL, err := bucket.SignURL(key, oss.HTTPGet, 600)
	if err != nil {
		code = constant.ERROR_OSS
		return
	}

	return signedGetURL, code, err
}

func (service UploadService) UploadImage() serializer.Response {
	code := constant.SUCCESS
	client, err := oss.New(os.Getenv("OSS_END_POINT"), os.Getenv("OSS_ACCESS_KEY_ID"), os.Getenv("OSS_ACCESS_KEY_SECRET"))
	if err != nil {
		logging.Info(err)
		code = constant.ERROR_OSS
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}

	// 获取存储空间。
	bucket, err := client.Bucket(os.Getenv("OSS_BUCKET"))
	if err != nil {
		logging.Info(err)
		code = constant.ERROR_OSS
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}

	// 获取扩展名
	ext := filepath.Ext(service.Filename)

	// 带可选参数的签名直传。
	options := []oss.Option{
		oss.ContentType(mime.TypeByExtension(ext)),
		oss.SetHeader("Access-Control-Allow-Origin", "*"),
	}

	key := "Upload/Image/" + uuid.Must(uuid.NewRandom()).String() + ext
	// 签名直传
	signedPutURL, err := bucket.SignURL(key, oss.HTTPPut, 600, options...)
	if err != nil {
		logging.Info(err)
		code = constant.ERROR_OSS
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}
	// 查看图片
	signedGetURL, err := bucket.SignURL(key, oss.HTTPGet, 600)
	if err != nil {
		logging.Info(err)
		code = constant.ERROR_OSS
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data: map[string]string{
			"key":    key,
			"putURL": signedPutURL,
			"getURL": signedGetURL,
		},
	}
}
