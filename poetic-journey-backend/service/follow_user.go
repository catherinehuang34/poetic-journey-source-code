package service

import (
	"poetic-journey/constant"
	"poetic-journey/model"
	"poetic-journey/serializer"
	"poetic-journey/util/logging"
	"time"
)

//
// TODO 需验证是否为登录用户本人的操作。可以用jwt里的ID。
type FollowUserService struct {
	FollowerID int  `json:"followerID" binding:"required"`
	FolloweeID int  `json:"followeeID" binding:"required"`
	Type       *int `json:"type" binding:"required"` // 0:follow 1:unfollow
}

//
type FollowUserCheckService struct {
	UserID    int   `form:"userID" binding:"required"`
	TargetIDs []int `form:"targetIDs" binding:"required"`
}

// 关注用户
func (service *FollowUserService) FollowUser() serializer.Response {
	code := constant.SUCCESS

	// 是否已关注
	followed, code, err := checkFollowUser(service.FollowerID, service.FolloweeID)
	if err == nil {
		if *service.Type == 0 && followed {
			return serializer.Response{
				Status: constant.INVALID_PARAMS,
				Msg:    "已关注该用户了哦",
			}
		}
		if *service.Type == 1 && !followed {
			return serializer.Response{
				Status: constant.INVALID_PARAMS,
				Msg:    "还没有关注该用户哦",
			}
		}
	}

	// 功能
	if err == nil {
		if *service.Type == 0 { // Follow
			err = model.DB.Exec(
				`insert into follow(follower_id, followee_id, follow_time)
					values(?, ?, ?)`,
				service.FollowerID, service.FolloweeID, time.Now()).Error
		} else { // Unfollow
			err = model.DB.Exec(
				`delete from follow
				where follower_id = ? and followee_id = ?`,
				service.FollowerID, service.FolloweeID).Error
		}

		// update user.follower
		val := -1 // unfollow
		if *service.Type == 0 {
			val = 1 // follow
		}
		if err == nil {
			err = model.DB.Exec(
				`update user
				set followers = followers + ?
				where user_id = ?`,
				val, service.FolloweeID).Error
		}
		if err == nil {
			err = model.DB.Exec(
				`update user
				set followees  = followees + ?
				where user_id = ?`,
				val, service.FollowerID).Error
		}
	}

	if err != nil {
		// 数据库错误
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
	}
}

// 检查是否已关注某用户
// 返回：followed []bool
func (service *FollowUserCheckService) CheckFollowUser() serializer.Response {
	code := constant.SUCCESS
	followed := make([]bool, len(service.TargetIDs))

	for i, v := range service.TargetIDs {
		res, code, err := checkFollowUser(service.UserID, v)
		if err != nil {
			logging.Info(err)
			return serializer.Response{
				Status: code,
				Msg:    constant.GetMsg(code),
			}
		}
		followed[i] = res
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   followed,
	}
}

func checkFollowUser(followerID, followeeID int) (followed bool, code int, err error) {
	code = constant.SUCCESS
	followed = true

	count := 0
	err = model.DB.Raw(
		`select count(*) from follow
		where follower_id = ? and followee_id = ?`,
		followerID, followeeID).Scan(&count).Error

	if err != nil {
		// 无记录
		// if errors.Is(err, gorm.ErrRecordNotFound) {
		// 	followed = false
		// 	err = nil
		// 	return
		// }

		// 数据库错误
		code = constant.ERROR_DATABASE
		return
	}

	followed = count > 0
	return
}
