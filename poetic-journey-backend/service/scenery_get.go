package service

import (
	"poetic-journey/constant"
	"poetic-journey/model"
	"poetic-journey/serializer"
	"poetic-journey/util/logging"
)

// 获取景点详细信息
type SceneryGetByIDService struct {
	SceneryID int `form:"sceneryID"`
}

// 获取景点列表信息
type SceneryGetByDetailService struct {
	Province string `form:"province" binding:"required"` // get params使用form 而非json
	City     string `form:"city"`
	District string `form:"district"`
	Keyword  string `form:"keyword"`
	Offset   int    `form:"offset"`
	Limit    int    `form:"limit"`
}

// 根据标签获取景点列表信息
type SceneryGetByTagService struct {
	Province string `form:"province"` // get params使用form 而非json
	City     string `form:"city"`
	District string `form:"district"`
	Tag      string `form:"tag"`
	Offset   int    `form:"offset"`
	Limit    int    `form:"limit"`
}

// 根据ID，获取景点所有信息
func (service *SceneryGetByIDService) GetSceneryByID() serializer.Response {
	code := constant.SUCCESS
	var scenery model.Scenery

	err := model.DB.Raw(
		`select * from scenery
		where scenery_id = ?`,
		service.SceneryID).Scan(&scenery).Error

	var pics []string
	if err == nil {
		pics, code, err = getSceneryPictures(service.SceneryID)
	}

	if err != nil {
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   serializer.BuildScenery(scenery, pics),
	}
}

// 根据省市区关键词，获取景点简单信息（在列表中的信息）
func (service *SceneryGetByDetailService) GetSceneryByDetail() serializer.Response {
	sceneries, code, err := service.getSceneryByDetail()

	var covers []string
	if err == nil {
		for _, scenery := range sceneries {
			str, code2, err2 := getSceneryCover(scenery.SceneryID)
			if err2 != nil {
				err = err2
				code = code2
				break
			}
			covers = append(covers, str)
		}
	}

	if err != nil {
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   serializer.BuildSceneriesBrief(sceneries, covers),
	}
}

// 根据省市区和标签，获取景点简单信息（在列表中的信息）
func (service *SceneryGetByTagService) GetSceneryByTag() serializer.Response {
	service.check()

	sceneries := make([]model.Scenery, 0)

	code := constant.SUCCESS
	var err error

	if IsEmptyAddress(service.Province) {

	} else if IsEmptyAddress(service.City) {
		err = model.DB.Raw(
			`select scenery.scenery_id, scenery.scenery_name, scenery.province, scenery.city, scenery.district, scenery.street, scenery.level, scenery.price, scenery.score_sum, scenery.score_count
			from scenery natural join scenery_tag natural join tag_table
			where province = ?  and tag = ?
			limit ?, ?`,
			service.Province, service.Tag, service.Offset, service.Limit).Scan(&sceneries).Error
	} else if IsEmptyAddress(service.District) {
		err = model.DB.Raw(
			`select scenery.scenery_id, scenery.scenery_name, scenery.province, scenery.city, scenery.district, scenery.street, scenery.level, scenery.price, scenery.score_sum, scenery.score_count
			from scenery natural join scenery_tag natural join tag_table
			where province = ?  and city = ? and tag = ?
			limit ?, ?`,
			service.Province, service.City, service.Tag, service.Offset, service.Limit).Scan(&sceneries).Error
	} else {
		err = model.DB.Raw(
			`select scenery.scenery_id, scenery.scenery_name, scenery.province, scenery.city, scenery.district, scenery.street, scenery.level, scenery.price, scenery.score_sum, scenery.score_count
			from scenery natural join scenery_tag natural join tag_table
			where province = ?  and city = ? and district = ? and tag = ?
			limit ?, ?`,
			service.Province, service.City, service.District, service.Tag, service.Offset, service.Limit).Scan(&sceneries).Error
	}

	var covers []string
	if err == nil {
		for _, scenery := range sceneries {
			str, code2, err2 := getSceneryCover(scenery.SceneryID)
			if err2 != nil {
				err = err2
				code = code2
				break
			}
			covers = append(covers, str)
		}
	}

	if err != nil {
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   serializer.BuildSceneriesBrief(sceneries, covers),
	}
}

// 获取景点名称列表（以及景点ID）
func (service *SceneryGetByDetailService) GetSceneryNameByDetail() serializer.Response {
	sceneries, code, err := service.getSceneryByDetail()

	if err != nil {
		logging.Info(err)
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   serializer.BuildSceneriesName(sceneries),
	}
}

func (service *SceneryGetByDetailService) check() {
	// TODO 使用address表获取地址，在address表中更改该值
	if service.Province == "香港特别行政区" {
		service.Province = "香港"
	}
	if service.City == "上海城区" {
		service.City = "上海市"
	}
	if service.City == "北京城区" {
		service.City = "北京市"
	}

	if service.Limit == 0 {
		service.Limit = 100
	}
}

func (service *SceneryGetByTagService) check() {
	// TODO 使用address表获取地址，在address表中更改该值
	if service.Province == "香港特别行政区" {
		service.Province = "香港"
	}
	if service.City == "上海城区" {
		service.City = "上海市"
	}
	if service.City == "北京城区" {
		service.City = "北京市"
	}

	if service.Limit == 0 {
		service.Limit = 10
	}
}

func IsEmptyAddress(str string) bool {
	return str == "" || str == "请选择"
}

// 根据省市区关键词，获取景点所有信息
func (service *SceneryGetByDetailService) getSceneryByDetail() (sceneries []model.Scenery, code int, err error) {
	service.check()

	code = constant.SUCCESS
	// println(service.Province, service.City, service.District, service.Keyword)

	if IsEmptyAddress(service.Province) {
		return sceneries, constant.INVALID_PARAMS, nil
	}
	if IsEmptyAddress(service.City) {
		err = model.DB.Raw(
			`select * from scenery
			where province = ?
			and (scenery_name like concat('%', ?, '%') or ? like concat('%', scenery_name, '%'))
			limit ?, ?`,
			service.Province, service.Keyword, service.Keyword, service.Offset, service.Limit).Scan(&sceneries).Error
	} else if IsEmptyAddress(service.District) {
		err = model.DB.Raw(
			`select * from scenery
			where province = ? and city = ?
			and (scenery_name like concat('%', ?, '%') or ? like concat('%', scenery_name, '%'))
			limit ?, ?`,
			service.Province, service.City, service.Keyword, service.Keyword, service.Offset, service.Limit).Scan(&sceneries).Error
	} else {
		err = model.DB.Raw(
			`select * from scenery
			where province = ? and city = ? and district = ?
			and (scenery_name like concat('%', ?, '%') or ? like concat('%', scenery_name, '%'))
			limit ?, ?`,
			service.Province, service.City, service.District, service.Keyword, service.Keyword, service.Offset, service.Limit).Scan(&sceneries).Error
	}
	return sceneries, code, err
}
