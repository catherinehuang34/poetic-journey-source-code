package service

import (
	"poetic-journey/constant"
	"poetic-journey/model"
	"poetic-journey/serializer"
	"poetic-journey/util/logging"
)

// 获取卡片评论列表
type GetCommentsService struct {
	CardID int `form:"cardID" binding:"required"`
	// Offset int  `form:"offset"` // 全部获取
	// Limit  int  `form:"limit"`
}

// 获取卡片评论列表
// 返回: comments []serializer.Comment
func (service *GetCommentsService) GetComments() serializer.Response {
	comments := make([]serializer.Comment, 0)

	code := constant.SUCCESS

	err := model.DB.Raw(
		"select * from comment where card_id = ?", service.CardID).Scan(&comments).Error

	if err == nil {
		type Result struct {
			Username string
			Avatar   string
		}
		for i, v := range comments {
			var res Result
			err = model.DB.Raw(
				"select username, avatar from user where user_id = ?", v.UserID).Scan(&res).Error
			if err != nil {
				break
			}
			comments[i].Username = res.Username
			comments[i].Avatar = GenerateAvatar(res.Avatar, v.UserID)
		}
	}

	if err != nil {
		// 数据库错误
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   comments,
	}
}
