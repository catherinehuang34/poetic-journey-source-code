package service

import (
	"poetic-journey/constant"
	"poetic-journey/model"
	"poetic-journey/serializer"
	"poetic-journey/util/logging"
	"time"
)

// 发表对卡片的评论
type PostCommentService struct {
	CardID int `json:"cardID" binding:"required"`
	UserID int `json:"userID" binding:"required"`

	CommentText string `json:"commentText" binding:"required"`
}

// 发表对卡片的评论
func (service *PostCommentService) PostComment() serializer.Response {
	code := constant.SUCCESS

	count := 0
	err := model.DB.Raw(
		"select count(*) from comment where card_id = ?", service.CardID).Scan(&count).Error

	if err == nil {
		err = model.DB.Exec(
			"insert into comment values(?, ?, ?, ?, ?)", service.CardID, count+1, service.UserID, service.CommentText, time.Now()).Error
	}

	if err != nil {
		// 数据库错误
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
	}
}
