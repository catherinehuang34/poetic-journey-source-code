package service

import (
	"poetic-journey/constant"
	"poetic-journey/model"
	"poetic-journey/serializer"
	"poetic-journey/util/logging"
)

// 用户新建景点
type CreateSceneryService struct {
	SceneryName string `json:"sceneryName" binding:"required"`

	Province string `json:"province" binding:"required"`
	City     string `json:"city" binding:"required"`
	District string `json:"district"`
	Street   string `json:"street"`

	Intro    string  `json:"intro"`
	OpenTime string  `json:"openTime"`
	Price    float32 `json:"price"`                    // // 暂不使用 默认为0？
	Score    float32 `json:"score" binding:"required"` // 上传者的评分，权重为3

	Pics []string `json:"pics"` // OSS keys
}

// 用户新建景点
// 返回: 新建后的景点id
func (service *CreateSceneryService) CreateScenery() serializer.Response {
	service.check()

	code := constant.SUCCESS

	var id int
	err := model.DB.Raw(`select max(scenery_id) from scenery`).Scan(&id).Error
	id++

	err = model.DB.Exec(
		`insert into scenery(scenery_id, scenery_name, province, city, district, street, intro, open_time, level, price, score_sum, score_count)
		values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
		id, service.SceneryName, service.Province, service.City, service.District, service.Street, service.Intro,
		service.OpenTime, "用户创建", service.Price, service.Score*3, 3).Error

	for i, v := range service.Pics {
		if err != nil {
			break
		}
		err = model.DB.Exec(
			`insert into scenery_picture
			values(?, ?, ?)`, id, i+1, v).Error
	}

	if err != nil && false {
		logging.Info(err)
		code = constant.ERROR_DATABASE
		return serializer.Response{
			Status: code,
			Msg:    constant.GetMsg(code),
			Error:  err.Error(),
		}
	}

	return serializer.Response{
		Status: code,
		Msg:    constant.GetMsg(code),
		Data:   id,
	}
}

func (service *CreateSceneryService) check() {
	if service.Province == "香港特别行政区" {
		service.Province = "香港"
	}
	if service.City == "上海城区" {
		service.City = "上海市"
	}
	if service.City == "北京城区" {
		service.City = "北京市"
	}
}
